import pandas as pd
import numpy as np
import re
import gc
from datetime import datetime as dt
from datetime import timedelta

import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from beauty import Beauty
import libpbeastpy

fmt = '%Y-%m-%d %H:%M:%S'

two_hrs_in_us = int(2*60*60*1e6)                                 # 2 hrs * 60 min * 60 s * 1e6 microseconds
number_of_chambers = 1112

# setup pbeast python API

db = libpbeastpy.ServerProxy('http://pc-tbed-bst-prod:8080')
beauty = Beauty('http://pc-tbed-bst-prod:8080')

def ts_convert(ts):
    
    #convert timestamp to date minus two hours (to correspond with dcs data times)
    tmp = dt.fromtimestamp((ts/1000000000)-7200)
    return tmp

# libpbeastpy query with time sort and 

def query_lib_data(since_ts, till_ts, query, source = 'DCS', type = 'number'):
    
    queryData = db.get_data('DCS', 
                            f'ATLAS_PVSS{source}', 
                            f'value-{type}', 
                            query, 
                            True, 
                            since_ts, 
                            till_ts, 
                            0, 
                            True)

    df = queryDataToDataFrame(queryData)
    df.sort_values(by=['ts'], inplace=True)
    df['dt'] = df['ts'].apply(lambda x : pd.Timestamp(int(x)*1000, tz='Europe/Zurich'))
    # df.drop(columns = 'ts', inplace = True)
    
    if type == 'number':
        
        df['value'] = df['value'].astype(float)
    
    return df

def link_creator(run_num, partition, chamber, hist, type = 'SHIFT'):

    link = f"https://atlasdaq.cern.ch/info/mda/get/MDTGnam/r0000{run_num}_lEoR_ATLAS_MDT-MDA-Monitoring_MDT.root//Histogramming-MDT/MDT-{partition}-GnamMon/{type}/MDT/{chamber}/{hist}"

#st.image(f"https://atlasdaq.cern.ch/info/mda/get/MDTGnam/r0000456729_lEoR_ATLAS_MDT-MDA-Monitoring_MDT.root//Histogramming-MDT/MDT-BA01-GnamMon/EXPERT/MDT/BIL1A03/HitsPerChannel",width=400)
    
    return link

def time_plotly(tsys, since_dt, till_dt, title, y_title, y_range = False, x_title = 'Date (CEST)', ybound_scale=.1, avg = False):
    
    max_ys = []
    min_ys = []
    
    fig = go.Figure()

    for t, y, l in tsys:
        
        max_ys.append(max(y))
        min_ys.append(min(y))
        fig.add_trace(go.Scatter(name = l, 
                                 mode="lines+markers", 
                                 x = t, 
                                 y = y))

    if avg != False:

        fig.add_hline(y = avg,
                      line_dash="dot",
                      line_color = 'red',
                      opacity=0.6,
                      name = f'Average {round(avg, 3)}',
                      annotation_text = "Average", 
                      annotation_position = "top left",
                      annotation_font_size = 15,
                      annotation_font_color = "red",
                      annotation_opacity = .75)

    fig.update_traces(
        opacity = .87
    )
    
    fig.update_layout(
        
        autosize = False,
        width = 1000,
        height = 750,
        title = title,
        xaxis_title = x_title,
        yaxis_title = y_title,
        template = 'plotly_dark'
    )

    if y_range == True:

        if len(max_ys) != 0 and len(min_ys) != 0:
        
            max_y = max(max_ys)
            min_y = min(min_ys)
            
            fig.update_layout(
                
            yaxis_range=[min_y*(1-ybound_scale), max_y*(1+ybound_scale)]
            
        )

    return fig

def lumiblock_plotly(tsys, 
                     fsm_array, 
                     since_dt, 
                     till_dt, 
                     title = '', 
                     y_title = '', 
                     y_range = False, 
                     x_title = 'Date (CEST)', 
                     ybound_scale=.1, 
                     sb_start = None, 
                     sb_end = None, 
                     dt = False,
                     percentages = None):

    if dt == True:

        dt = 'dt'

    else:

        dt = 'Number'
    
    max_ys = []
    min_ys = []

    y_offset = .03
    y_counter = 1
    
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    for t, y, l in tsys:
        
        max_ys.append(max(y))
        min_ys.append(min(y))
        fig.add_trace(go.Scatter(name = l, mode="lines+markers", x = t, y = y),
                     secondary_y=False)

    max_y = max(max_ys)
    min_y = min(min_ys)

    if sb_start != None:

        for start, end in zip(sb_start, sb_end):

            fig.add_vrect(x0=start, 
                          x1=end, 
                          line_width = 0, 
                          fillcolor="yellow", 
                          opacity=0.15)
    
            fig.add_vline(
                    x = start,
                    line_dash="dot",
                    line_color = 'yellow',
                    opacity=0.6
                    )
    
            fig.add_vline(
                    x = end,
                    line_dash="dot",
                    line_color = 'yellow',
                    opacity=0.6
                    )
            
        fig.add_trace(go.Scatter(name = 'Stable Beam Range', 
                                     mode="lines", 
                                     line_dash="dot",
                                     line_color = 'yellow', 
                                     x = [start], 
                                     y = [0], 
                                     opacity=0.6),
                     secondary_y=False)    

    if percentages != None:

        for t, y, l in percentages:
        
            fig.add_trace(go.Scatter(name = l, mode="lines+markers", x = t, y = y),
                         secondary_y=True)
            
    lbn_prev = -1

    # for i in range(fsm_array.shape[0]):

        # if lbn_prev == fsm_array.iloc[i]['LumiBlock Number']:

        #     name = str(fsm_array.iloc[i]['Chamber']) + ' - ' + str(fsm_array.iloc[i]['Info']) + ' - ' + str(fsm_array.iloc[i]['FSM Flag'])
        #     text = str(fsm_array.iloc[i]['Chamber']) + ' - ' + str(fsm_array.iloc[i]['Info']) + ' - ' + str(fsm_array.iloc[i]['FSM Flag'])
        
        #     fig.add_trace(go.Scatter(name = name, 
        #                              text = text, 
        #                              mode="lines+markers", 
        #                              x = [fsm_array.iloc[i][f'LumiBlock {dt}']], 
        #                              y = [(0.1 + y_counter * y_offset) * max_y],
        #                              marker = dict(color = 'red', size = 7)), 
        #                   secondary_y=False)

        #     y_counter += 1

        # else:

        #     fig.add_vline(
        #     x = fsm_array.iloc[i][f'LumiBlock {dt}'],
        #     line_dash="dot",
        #     line_color = 'red'
        #     )

        #     name = str(fsm_array.iloc[i]['Chamber']) + ' - ' + str(fsm_array.iloc[i]['Info']) + ' - ' + str(fsm_array.iloc[i]['FSM Flag'])
        #     text = str(fsm_array.iloc[i]['Chamber']) + ' - ' + str(fsm_array.iloc[i]['Info']) + ' - ' + str(fsm_array.iloc[i]['FSM Flag'])

        #     fig.add_trace(go.Scatter(name = name,
        #                              text = text, 
        #                              mode="lines+markers", 
        #                              x = [fsm_array.iloc[i][f'LumiBlock {dt}']], 
        #                              y = [0.1 * max_y],
        #                              marker=dict(color='red', size=7)),
        #                   secondary_y=False)

        #     y_counter = 1

        # lbn_prev = fsm_array.iloc[i]['LumiBlock Number']
    
    for i in range(fsm_array.shape[0]):

        if lbn_prev != fsm_array.iloc[i]['LumiBlock Number']:

            fig.add_vline(
            x = fsm_array.iloc[i][f'LumiBlock {dt}'],
            line_dash="dot",
            line_color = 'red'
            )

        lbn_prev = fsm_array.iloc[i]['LumiBlock Number']

    fig.update_traces(opacity = .87)
    
    fig.update_layout(
        
        autosize = False,
        width = 1000,
        height = 750,
        title = title,
        xaxis_title = x_title,
        yaxis_title = y_title,
        template = 'plotly_dark'
        
    )

    fig.update_yaxes(autorangeoptions=dict(maxallowed=100), 
                     title_text="FSM DQ Percentage", 
                     secondary_y=True, 
                     tickmode = 'sync')
    
    # fig.update_yaxes(rangemode='tozero', scaleanchor='y', scaleratio=1, constraintoward='bottom', secondary_y=False)
    # fig.update_yaxes(rangemode='tozero', scaleanchor='y2', scaleratio=1, constraintoward='bottom', secondary_y=True)

    if y_range == True:
        
        fig.update_layout(
            
        yaxis_range=[min_y*(1-ybound_scale), max_y*(1+ybound_scale)]
        
    )

    return fig

def lumi_plotly(hv_lumi, chamber_query, lumi_query, layer):

    title = f"ML{layer} iMon vs. Luminosity <br><sup>{chamber_query} vs. {lumi_query}</sup>"

    fig = go.Figure()
    
    fig.add_trace(go.Scatter(mode="markers", x = hv_lumi.index, y = hv_lumi.values, marker = dict(color = 'blue', opacity = .45)))
    
    fig.update_layout(
        autosize = False,
        width = 750,
        height = 750,
        title = go.layout.Title(text=title,
                                xref="paper",
                                x=0),
        xaxis_title = 'Luminosity',
        yaxis_title = 'Current (μA)',
        template = 'plotly_dark'
    )
    
    return fig

# Function to create the dataframe from queryData
def queryDataToDataFrame(queryData):   
    
    # Define a regular expression pattern to match the timestamp and value
    pattern = r"DataPoint\(ts=(\d+), value=('.*?'|[0-9.]+)\)"
    row =[]
    try:
        for key in queryData[0].data.keys():
            for x in queryData[0].data[key]:
                # Use re.match to find the values
                match = re.match(pattern, str(x))
                if match:
                    timestamp = match.group(1)
                    value = match.group(2)
                    #print(f"Timestamp: {timestamp}, Value: {value}")
                else:
                    print("The string does not match the expected format.")
                    return
                row.append((key,timestamp,value))
                
        # Define column names
        column_names = ['datapoint', 'ts', 'value']
    
        # Create the DataFrame using the list of tuples
        df = pd.DataFrame(row, columns = column_names)

    except IndexError:

        df = pd.DataFrame({'datapoint': np.array([], dtype=str), 'ts' : np.array([], dtype=int), 'value' : np.array([], dtype=int)})
        
    return df

# def generate_rn_df(since_dt, till_dt):

#     rn_df = beauty.timeseries(since_dt, till_dt, 'DCS', 'ATLAS_PVSSDCS', 'value-number', 'ATLGCSDDC:daqRunNumber.value',None, False)[0]
#     rn_df = pd.DataFrame({'value': rn_df.array , 'dt': rn_df.index.tz_localize(None)})
#     rn_df['value'] = rn_df['value'].astype(float).astype(int)
#     rn_df['dt'] = rn_df['dt'] + pd.DateOffset(hours = 2)
#     rn_df['ts'] = rn_df['dt'].values.astype(int) // 10**3
    
#     s_e = []
    
#     for k in range(rn_df.shape[0] - 1):
    
#         if rn_df.iloc[k]['value'] == rn_df.iloc[k+1]['value']:
    
#             s_e.append('Start')
    
#         else:
    
#             s_e.append('End')
    
#     if s_e[-1] == 'Start':
    
#         s_e.append('End')
    
#     else:
    
#         s_e.append('Start')
    
#     rn_df['Start/End'] = pd.Series(s_e)

#     return rn_df

def generate_rn_sb_df(since_dt, till_dt):

    rn_df = beauty.timeseries(since_dt, 
                              till_dt, 
                              'DCS', 
                              'ATLAS_PVSSDCS', 
                              'value-number', 
                              'ATLGCSDDC:daqRunNumber.value', 
                              None, 
                              False)[0]
    
    rn_df = pd.DataFrame({'value': rn_df.array , 'dt': rn_df.index})
    rn_df['value'] = rn_df['value'].astype(float).astype(int)
    rn_df['ts'] = rn_df['dt'].values.astype(int) // 10**3
    
    for i in rn_df['value'].unique():
        
        if rn_df[rn_df['value'] == i].shape[0] != 2:
    
            rn_df.drop(rn_df[rn_df['value'] == i].index[0], inplace = True)
    
    total_runs = int(rn_df.shape[0]/2)
    
    sb_df = beauty.timeseries(since_dt, 
                               till_dt, 
                               'DCS', 
                               'ATLAS_PVSSDCS', 
                               'value-number', 
                               'ATLGCSLHC:bit_lhc_RunCtrl_SafeBeam_StableBeams.value', 
                               None, 
                               False)[0]
    
    sb_df = pd.DataFrame({'value': sb_df.array , 'dt': sb_df.index})
    sb_df['value'] = sb_df['value'].astype(float).astype(int)
    sb_df['ts'] = sb_df['dt'].values.astype(int) // 10**3

    rns = []
    rn_start_tss = []
    rn_end_tss = []
    
    for i in range(0, rn_df.shape[0], 2):
    
        rn_tmp_df = rn_df.iloc[i:i+2]
    
        # print(rn_tmp_df)
    
        if rn_tmp_df.shape[0] == 2:
    
            rn = rn_tmp_df['value'].iloc[0]
            rn_start_ts = rn_tmp_df['ts'].iloc[0]
            rn_end_ts = rn_tmp_df['ts'].iloc[1]
    
            tmp_df = sb_df[(sb_df['ts'] > rn_start_ts) & (sb_df['ts'] < rn_end_ts)]
    
            # print(tmp_df)
    
            if tmp_df.shape[0] > 0:
        
                rns.append(rn)
                rn_start_tss.append(rn_start_ts)
                rn_end_tss.append(rn_end_ts)
    
        else:
    
            continue
    
    dic_tmp = {'Stable Beam Run Number': rns, 
               'Run Start ts': rn_start_tss, 
               'Run End ts': rn_end_tss
              }
    
    rn_sb_df = pd.DataFrame(dic_tmp)
    rn_sb_df['Run Start dt'] = pd.to_datetime(rn_sb_df['Run Start ts'], unit = 'us').dt.tz_localize('UTC').dt.tz_convert('Europe/Zurich')
    rn_sb_df['Run End dt'] = pd.to_datetime(rn_sb_df['Run End ts'], unit = 'us').dt.tz_localize('UTC').dt.tz_convert('Europe/Zurich')
    rn_sb_df.drop(columns = ['Run Start ts', 'Run End ts'], inplace = True)
    rn_sb_df = rn_sb_df.iloc[::-1].reset_index(drop=True)
    
    # percentage_of_stable_beam_runs = round(len(rn_sb_df) / total_runs) * 100, 2)

    return rn_sb_df

def generate_lumi_lbn_df(since_ts, till_ts):

    lumi2 = query_lib_data(since_ts, till_ts, 'ATLGCSLUMI:ATLAS_PREFERRED_LBAv_PHYS_CalibLumi.value')
    lumi2['value'] = lumi2['value'].astype('float64')

    lbn2 = query_lib_data(since_ts, till_ts, 'ATLGCSDDC:daqLumiBlockNumber.value')
    lbn2['value'] = lbn2['value'].astype('float64')

    lbns = []
    lbn_tss = []
    lumis = []
    lumi_tss = []
    
    j = 0
    
    for i in range(lbn2.shape[0] - 1):
    
        lbn_ts = lbn2['ts'].iloc[i]
    
        lumi_ts = lumi2['ts'].iloc[j]
    
        while lumi_ts < lbn_ts:
    
            if j < lumi2.shape[0] - 1:
    
                j += 1
            
                lumi_ts = lumi2['ts'].iloc[j]
    
            else:
    
                break
        
        lbns.append(lbn2['value'].iloc[i])
        lbn_tss.append(lbn_ts)
        lumis.append(lumi2['value'].iloc[j])
        lumi_tss.append(lumi_ts)
    
    
    dic_tmp = {'LumiBlock Number' : lbns, 'LumiBlock ts' : lbn_tss, 'Luminosity' : lumis, 'Luminosity ts' : lumi_tss}
    
    lbn_lumi_df = pd.DataFrame(dic_tmp)
    lbn_lumi_df['LumiBlock dt'] = pd.to_datetime(lbn_lumi_df['LumiBlock ts'], unit = 'us').dt.tz_localize('UTC').dt.tz_convert('Europe/Zurich')


    return lbn_lumi_df

# Function to create a dataframe with LumiBlock number, StableBeam data, Ready4Physics data, and LumiBlock timestamp
# Uses Beauty
def generate_lbn_sb_r4p_df(since_dt, till_dt):
    
    lbn_df = beauty.timeseries(since_dt + timedelta(hours = 2), 
                            till_dt + timedelta(hours = 2), 
                            'DCS', 
                            'ATLAS_PVSSDCS', 
                            'value-number', 
                            'ATLGCSDDC:daqLumiBlockNumber.value', 
                            None, 
                            False)[0]
    sb_df = beauty.timeseries(since_dt + timedelta(hours = 2), 
                               till_dt + timedelta(hours = 2), 
                               'DCS', 
                               'ATLAS_PVSSDCS', 
                               'value-number', 
                               'ATLGCSLHC:bit_lhc_RunCtrl_SafeBeam_StableBeams.value', 
                               None, 
                               False)[0]

    try:
        
        r4p_df = beauty.timeseries(since_dt + timedelta(hours = 2), 
                                    till_dt + timedelta(hours = 2), 
                                    'DCS', 
                                    'ATLAS_PVSSDCS', 
                                    'value-number', 
                                    'ATLGCSLHC:ATLAS.Ready4Physics', 
                                    None, 
                                    False)[0]

        lbn_df = pd.DataFrame({'value': lbn_df.array, 'dt': lbn_df.index})
        sb_df = pd.DataFrame({'value': sb_df.array , 'dt': sb_df.index})
        r4p_df = pd.DataFrame({'value': r4p_df.array , 'dt': r4p_df.index})
    
        i = 0
        j = 0
        k = 0
    
        lbn = lbn_df.iloc[i]['value']
        sb = sb_df.iloc[j]['value']
        r4p = r4p_df.iloc[k]['value']
    
        lbn_ts = int(dt.timestamp(lbn_df.iloc[i]['dt'])*int(1e6))
        lbn_ts_prev = lbn_ts
        sb_ts = int(dt.timestamp(sb_df.iloc[j]['dt'])*int(1e6))    
        sb_ts_prev = lbn_ts
        r4p_ts = int(dt.timestamp(r4p_df.iloc[k]['dt'])*int(1e6))
        r4p_ts_prev = lbn_ts
    
        if sb_ts > lbn_ts:
        
            if sb == 1.0:
        
                sb = 0.0
        
            else:
        
                sb = 1.0
        
        if r4p_ts > lbn_ts:
        
            if r4p == 1.0:
        
                r4p = 0.0
        
            else:
        
                r4p = 1.0
    
        lbns = [lbn]
        lbn_tss = [lbn_ts]
        sbs = [sb]
        sb_tss = [sb_ts]
        r4ps = [r4p]
        r4p_tss= [r4p_ts]
    
        for i in range(1, lbn_df.shape[0]):
        
            lbn_ts = int(dt.timestamp(lbn_df.iloc[i]['dt'])*int(1e6))
            
            if lbn_ts_prev < sb_ts < lbn_ts:
    
                sb_ts_prev = sb_ts
    
                sb = sb_df.iloc[j]['value']
    
                j += 1
    
                if j < sb_df.shape[0]:
    
                    sb_ts = int(dt.timestamp(sb_df.iloc[j]['dt'])*int(1e6))
        
            if lbn_ts_prev < r4p_ts < lbn_ts:
    
                r4p_ts_prev = r4p_ts
                
                r4p = r4p_df.iloc[k]['value']
    
                k += 1
    
                if k < r4p_df.shape[0]:
                
                    r4p_ts = int(dt.timestamp(r4p_df.iloc[k]['dt'])*int(1e6))
    
            lbns.append(lbn_df.iloc[i]['value'])
            sbs.append(sb)
            r4ps.append(r4p)
            lbn_tss.append(lbn_ts_prev)
            sb_tss.append(sb_ts_prev)
            r4p_tss.append(r4p_ts_prev)
            
            lbn_ts_prev = lbn_ts
        
        dic_tmp = {'LumiBlock Number': lbns, 
                   'StableBeam': sbs, 
                   'Ready4Physics': r4ps, 
                   'LumiBlock ts': lbn_tss, 
                   'StableBeam ts': sb_tss, 
                   'Ready4Physics ts': r4p_tss}
        
        lbn_sb_r4p_df = pd.DataFrame(dic_tmp)
        lbn_sb_r4p_df['LumiBlock Number'] = lbn_sb_r4p_df['LumiBlock Number'].astype(int)
        lbn_sb_r4p_df['StableBeam'] = lbn_sb_r4p_df['StableBeam'].astype(int)
        lbn_sb_r4p_df['Ready4Physics'] = lbn_sb_r4p_df['Ready4Physics'].astype(int)
        lbn_sb_r4p_df['LumiBlock dt'] = pd.to_datetime(lbn_sb_r4p_df['LumiBlock ts'], unit = 'us').dt.tz_localize('UTC').dt.tz_convert('Europe/Zurich')

    except: 

        lbn_df = pd.DataFrame({'value': lbn_df.array, 'dt': lbn_df.index})
        sb_df = pd.DataFrame({'value': sb_df.array , 'dt': sb_df.index})
    
        i = 0
        j = 0
    
        lbn = lbn_df.iloc[i]['value']
        sb = sb_df.iloc[j]['value']
    
        lbn_ts = int(dt.timestamp(lbn_df.iloc[i]['dt'])*int(1e6))
        lbn_ts_prev = lbn_ts
        sb_ts = int(dt.timestamp(sb_df.iloc[j]['dt'])*int(1e6))    
        sb_ts_prev = lbn_ts
    
        if sb_ts > lbn_ts:
        
            if sb == 1.0:
        
                sb = 0.0
        
            else:
        
                sb = 1.0
    
        lbns = [lbn]
        lbn_tss = [lbn_ts]
        sbs = [sb]
        sb_tss = [sb_ts]
    
        for i in range(1, lbn_df.shape[0]):
        
            lbn_ts = int(dt.timestamp(lbn_df.iloc[i]['dt'])*int(1e6))
            
            if lbn_ts_prev < sb_ts < lbn_ts:
    
                sb_ts_prev = sb_ts
    
                sb = sb_df.iloc[j]['value']
    
                j += 1
    
                if j < sb_df.shape[0]:
    
                    sb_ts = int(dt.timestamp(sb_df.iloc[j]['dt'])*int(1e6))
    
            lbns.append(lbn_df.iloc[i]['value'])
            sbs.append(sb)
            lbn_tss.append(lbn_ts_prev)
            sb_tss.append(sb_ts_prev)
            
            lbn_ts_prev = lbn_ts
        
        dic_tmp = {'LumiBlock Number': lbns, 
                   'StableBeam': sbs, 
                   'LumiBlock ts': lbn_tss, 
                   'StableBeam ts': sb_tss}
        
        lbn_sb_r4p_df = pd.DataFrame(dic_tmp)
        lbn_sb_r4p_df['LumiBlock Number'] = lbn_sb_r4p_df['LumiBlock Number'].astype(int)
        lbn_sb_r4p_df['StableBeam'] = lbn_sb_r4p_df['StableBeam'].astype(int)
        lbn_sb_r4p_df['LumiBlock dt'] = pd.to_datetime(lbn_sb_r4p_df['LumiBlock ts'], unit = 'us').dt.tz_localize('UTC').dt.tz_convert('Europe/Zurich')
        #lbn_sb_r4p_df.drop(columns = ['Ready4Physics ts'], inplace = True)
    
    while lbn_sb_r4p_df['LumiBlock Number'].iloc[0] != 1.0:

        lbn_sb_r4p_df = lbn_sb_r4p_df.iloc[1:]

    return lbn_sb_r4p_df

def generate_lbn_sb_fsm_flags_df(mdt_fsm_flags_df, lbn_sb_r4p_df):
        
    lbns = []
    fsms = []
    fsm_flags = []
    fsm_tss = []
    lbn_tss = []
    sbs = []
    r4ps = []
    rns = []
    
    i = 0
    k = 0
    
    
    for j in range(lbn_sb_r4p_df.shape[0]-1):
    
        lbn_ts_prev = lbn_sb_r4p_df.iloc[j]['LumiBlock ts']
        lbn_ts = lbn_sb_r4p_df.iloc[j+1]['LumiBlock ts']
        # fsm_ts = np.int64(mdt_fsm_flags_df.iloc[i]['ts'])
        # print(lbn_ts_prev,'\n',lbn_ts,'\n',fsm_ts, sep ='')
        # print(pd.Timestamp.fromtimestamp(lbn_ts_prev/1e6),'\n',lbn_ts,'\n',pd.Timestamp.fromtimestamp(fsm_ts/1e6), sep ='')
        # print(type(lbn_ts_prev),'\n',type(lbn_ts),'\n',type(fsm_ts), sep ='')
        # print(mdt_fsm_flags_df['ts'])

        df_tmp = mdt_fsm_flags_df[(lbn_ts_prev < mdt_fsm_flags_df['ts']) & (lbn_ts > mdt_fsm_flags_df['ts'])]    
    
        if df_tmp.shape[0] > 0:
    
            for k in df_tmp.values:
            
                lbns.append(lbn_sb_r4p_df.iloc[j]['LumiBlock Number'])
                fsms.append(k[0][6:-17])
                fsm_flags.append(k[2])
                sbs.append(lbn_sb_r4p_df.iloc[j]['StableBeam'])
                r4ps.append(lbn_sb_r4p_df.iloc[j]['Ready4Physics'])
                fsm_tss.append(k[1])
                lbn_tss.append(lbn_ts_prev)
    
                i += 1
                
    dic_tmp = {'LumiBlock Number': lbns, 
               'FSM Datapoint': fsms, 
               'FSM Flag': fsm_flags, 
               'Stable Beam': sbs, 
               'Ready 4 Physics': r4ps, 
               'LumiBlock ts': lbn_tss, 
               'FSM ts': fsm_tss}
    
    lbn_fsm_flags_df = pd.DataFrame(dic_tmp)
    lbn_fsm_flags_df['LumiBlock dt'] = pd.to_datetime(lbn_fsm_flags_df['LumiBlock ts'], unit = 'us').dt.tz_localize('UTC').dt.tz_convert('Europe/Zurich')
    lbn_fsm_flags_df['FSM dt'] = pd.to_datetime(lbn_fsm_flags_df['FSM ts'], unit = 'us').dt.tz_localize('UTC').dt.tz_convert('Europe/Zurich')
    lbn_fsm_flags_df.drop(columns = ['LumiBlock ts', 'FSM ts'], inplace = True)
    
    
    return lbn_fsm_flags_df

def generate_lbn_fsm_df(since_dt, till_dt, mdt_fsm_flags_df, chamber_list):
    
    lbn_df = beauty.timeseries(since_dt + timedelta(hours = 2), 
                               till_dt + timedelta(hours = 2)
                               , 
                               'DCS', 
                               'ATLAS_PVSSDCS', 
                               'value-number', 
                               'ATLGCSDDC:daqLumiBlockNumber.value', 
                               None, 
                               False)[0]
    
    lbn_df = pd.DataFrame({'value': lbn_df.array, 'dt': lbn_df.index})

    lbn_df['value'] = lbn_df['value'].astype(float).astype(int)
    lbn_df['ts'] = lbn_df['dt'].values.astype(int) // 10**3
    
    dic_tmp = {'LumiBlock Number': lbn_df['value'], 
               'LumiBlock ts': lbn_df['ts']}
    
    lbn_df = pd.DataFrame(dic_tmp)
    lbn_df['LumiBlock dt'] = pd.to_datetime(lbn_df['LumiBlock ts'], unit = 'us').dt.tz_localize('UTC').dt.tz_convert('Europe/Zurich')
        
    lbns = []
    fsms = []
    fsm_flags = []
    fsm_tss = []
    lbn_tss = []
    rns = []
    
    i = 0
    k = 0
    
    
    for j in range(lbn_df.shape[0]-1):
    
        lbn_ts_prev = lbn_df.iloc[j]['LumiBlock ts']
        lbn_ts = lbn_df.iloc[j+1]['LumiBlock ts']

        df_tmp = mdt_fsm_flags_df[(lbn_ts_prev < mdt_fsm_flags_df['ts']) & (lbn_ts > mdt_fsm_flags_df['ts'])]    
    
        if df_tmp.shape[0] > 0:
    
            for k in df_tmp.values:
            
                lbns.append(lbn_df.iloc[j]['LumiBlock Number'])
                fsms.append(k[0][6:-17])
                fsm_flags.append(k[2])
                fsm_tss.append(k[1])
                lbn_tss.append(lbn_ts_prev)
                    
    dic_tmp = {'LumiBlock Number': lbns, 
               'FSM Datapoint': fsms, 
               'FSM Flag': fsm_flags, 
               'LumiBlock ts': lbn_tss, 
               'FSM ts': fsm_tss}
    
    lbn_fsm_flags_df = pd.DataFrame(dic_tmp)
    lbn_fsm_flags_df['LumiBlock dt'] = pd.to_datetime(lbn_fsm_flags_df['LumiBlock ts'], unit = 'us').dt.tz_localize('UTC').dt.tz_convert('Europe/Zurich')
    lbn_fsm_flags_df['FSM dt'] = pd.to_datetime(lbn_fsm_flags_df['FSM ts'], unit = 'us').dt.tz_localize('UTC').dt.tz_convert('Europe/Zurich')
    lbn_fsm_flags_df.drop(columns = ['LumiBlock ts', 'FSM ts'], inplace = True)

    datapoints = lbn_fsm_flags_df['FSM Datapoint'].tolist()
    
    fsm_chambers = []
    fsm_partitions = []
    fsm_infos = []
    
    for datapoint in datapoints:
    
        if '_JTAG' in datapoint:
    
            chamb = datapoint[-7:]
            
            fsm_infos.append('JTAG')
            fsm_chambers.append(chamb)
            fsm_partitions.append(chamber_list[chamber_list['Chamber'] == chamb]['Partition'].values[0])
    
        elif '_ML1' in datapoint:
        
            chamb = datapoint[-11:-4]
            
            fsm_infos.append('ML1')
            fsm_chambers.append(chamb)
            fsm_partitions.append(chamber_list[chamber_list['Chamber'] == chamb]['Partition'].values[0])
    
        elif '_ML2' in datapoint:    
    
            chamb = datapoint[-11:-4]
            
            fsm_infos.append('ML2')
            fsm_chambers.append(chamb)
            fsm_partitions.append(chamber_list[chamber_list['Chamber'] == chamb]['Partition'].values[0])
    
        elif '_LV' in datapoint:
    
            chamb = datapoint[-10:-3]
            
            fsm_infos.append('LV')
            fsm_chambers.append(chamb)
            fsm_partitions.append(chamber_list[chamber_list['Chamber'] == chamb]['Partition'].values[0])

    lbn_fsm_flags_df['Chamber'] = fsm_chambers
    lbn_fsm_flags_df['Partition'] = fsm_partitions
    lbn_fsm_flags_df['Info'] = fsm_infos
    lbn_fsm_flags_df['Sector'] = [x[:2] for x in lbn_fsm_flags_df['Partition']]
    
    return lbn_fsm_flags_df

def generate_pie_charts(lbn_fsm_sb_flags_df, chamber_list, total_sb_lb):
    
    datapoints = lbn_fsm_sb_flags_df['FSM Datapoint'].tolist()
    
    fsm_chambers = []
    fsm_partitions = []
    fsm_infos = []
    
    for datapoint in datapoints:
    
        if '_JTAG' in datapoint:
    
            chamb = datapoint[-7:]
            
            fsm_infos.append('JTAG')
            fsm_chambers.append(chamb)
            fsm_partitions.append(chamber_list[chamber_list['Chamber'] == chamb]['Partition'].values[0])
    
        elif '_ML1' in datapoint:
        
            chamb = datapoint[-11:-4]
            
            fsm_infos.append('ML1')
            fsm_chambers.append(chamb)
            fsm_partitions.append(chamber_list[chamber_list['Chamber'] == chamb]['Partition'].values[0])
    
        elif '_ML2' in datapoint:    
    
            chamb = datapoint[-11:-4]
            
            fsm_infos.append('ML2')
            fsm_chambers.append(chamb)
            fsm_partitions.append(chamber_list[chamber_list['Chamber'] == chamb]['Partition'].values[0])
    
        elif '_LV' in datapoint:
    
            chamb = datapoint[-10:-3]
            
            fsm_infos.append('LV')
            fsm_chambers.append(chamb)
            fsm_partitions.append(chamber_list[chamber_list['Chamber'] == chamb]['Partition'].values[0])

    lbn_fsm_sb_flags_df['Chamber'] = fsm_chambers
    lbn_fsm_sb_flags_df['Partition'] = fsm_partitions
    lbn_fsm_sb_flags_df['Info'] = fsm_infos
    lbn_fsm_sb_flags_df['Sector'] = [x[:2] for x in lbn_fsm_sb_flags_df['Partition']]
    sectors = pd.Series([x[:2] for x in chamber_list['Partition'].unique().tolist()]).unique().tolist()
    sectors = [sectors[0], sectors[3], sectors[2], sectors[1]]
    infos = ['JTAG', 'ML1', 'ML2', 'LV']
    
    number_of_chambers_in_sectors = {}
    
    for sector in sectors:
    
        df = chamber_list[chamber_list.apply(lambda row: row.astype(str).str.contains(f'{sector}').any(), axis=1)]
    
        number_of_chambers_in_sectors[f'{sector}'] = df.shape[0]
    
    number_of_errors_in_sectors = {}
    
    for sector in sectors:
    
        number_of_errors_in_sector = lbn_fsm_sb_flags_df[lbn_fsm_sb_flags_df['Sector'] == sector].shape[0]
    
        infos_tmp = {}
    
        for info in infos:
    
             infos_tmp[f'{info}'] = (lbn_fsm_sb_flags_df[(lbn_fsm_sb_flags_df['Sector'] == sector) & (lbn_fsm_sb_flags_df['Info'] == info)].shape[0])
    
        number_of_errors_in_sectors[f'{sector}'] = [number_of_errors_in_sector, infos_tmp]
    
    fsm_percentage_for_sectors = {}
    
    for sector in sectors:
    
        percentage_of_flagged_chambers = len(lbn_fsm_sb_flags_df[lbn_fsm_sb_flags_df['Sector'] == sector]['LumiBlock Number'].unique())/(number_of_chambers_in_sectors[f'{sector}']*total_sb_lb)*100

        infos_tmp = {}
    
        for info in infos:

            if number_of_errors_in_sectors[f'{sector}'][0] != 0:
    
             infos_tmp[f'{info}'] = number_of_errors_in_sectors[f'{sector}'][1][f"{info}"]/number_of_errors_in_sectors[f'{sector}'][0]*percentage_of_flagged_chambers

            else:
                
                infos_tmp[f'{info}'] = 0
    
        fsm_percentage_for_sectors[f'{sector}'] = [percentage_of_flagged_chambers, infos_tmp]

    
    fsm_pie_charts = []
    
    # for sector in sectors:
        
    #     fsm_pie_chart = go.Figure(go.Sunburst(
    #         labels=[f"Functional", 
    #                 f"Flagged",
    #                 "JTAG",
    #                 "ML1",
    #                 "ML2",
    #                 "LV"],
    #         parents=[f"{sector}", 
    #                  f"{sector}", 
    #                  f"Flagged", 
    #                  f"Flagged",
    #                  f"Flagged",
    #                  f"Flagged"],
    #         values=[100-fsm_percentage_for_sectors[f'{sector}'][0], 
    #                 fsm_percentage_for_sectors[f'{sector}'][0],
    #                 fsm_percentage_for_sectors[f'{sector}'][1]["JTAG"],
    #                 fsm_percentage_for_sectors[f'{sector}'][1]["ML1" ],
    #                 fsm_percentage_for_sectors[f'{sector}'][1]["ML2" ],
    #                 fsm_percentage_for_sectors[f'{sector}'][1]["LV"  ]],
                    
    #         insidetextorientation = 'horizontal'
    #     ))
        
    #     fsm_pie_charts.append(fsm_pie_chart)

    return fsm_pie_charts, lbn_fsm_sb_flags_df, fsm_percentage_for_sectors

def generate_perfect_dq_pie_charts(chamber_list):

    sectors = pd.Series([x[:2] for x in chamber_list['Partition'].unique().tolist()]).unique().tolist()
    sectors = [sectors[0], sectors[3], sectors[2], sectors[1]]
    
    fsm_percentage_for_sectors = {}
    
    for sector in sectors:
    
        percentage_of_flagged_chambers = 0
    
        fsm_percentage_for_sectors[f'{sector}'] = percentage_of_flagged_chambers
    
    fsm_pie_charts = []
    
    # for sector in sectors:
        
    #     fsm_pie_chart = go.Figure(go.Sunburst(
    #         labels=[f"Functional", 
    #                 f"Flagged"],
    #         parents=[f"{sector}", 
    #                  f"{sector}"],
    #         values=[100, 
    #                 0],
                    
    #         insidetextorientation = 'horizontal'
    #     ))
        
    #     fsm_pie_charts.append(fsm_pie_chart)

    return fsm_pie_charts, fsm_percentage_for_sectors

def generate_fsm_percentages(lbn_sb_fsm_flags_df, sb_lb_start, sb_lb_end):

    fsm_sb_df = lbn_sb_fsm_flags_df[lbn_sb_fsm_flags_df['Stable Beam'] == 1]
    
    fsm_jtag_df = fsm_sb_df[fsm_sb_df['Info'] == 'JTAG']
    fsm_hv_df = fsm_sb_df[(fsm_sb_df['Info'] == 'ML1') | (fsm_sb_df['Info'] == 'ML2')]
    fsm_lv_df = fsm_sb_df[fsm_sb_df['Info'] == 'LV']

    sb_range = list(range(sb_lb_start, sb_lb_end))

    fsm_lbns = fsm_jtag_df['LumiBlock Number'].unique()
    
    j = 0
    
    fsm_jtag_percentages = []
    number_of_errors = 0
    
    for i in sb_range:
    
        if i in fsm_lbns:
    
            fsm_lbn = int(fsm_lbns[j])
    
            number_of_errors = fsm_jtag_df[fsm_jtag_df['LumiBlock Number'] == i].shape[0]
    
            fsm_jtag_percentage = (number_of_chambers - number_of_errors) / number_of_chambers * 100
    
            fsm_jtag_percentages.append(fsm_jtag_percentage)
    
            j += 1
    
        else:
    
            fsm_jtag_percentages.append(100)

    fsm_lbns = fsm_hv_df['LumiBlock Number'].unique()
    
    j = 0
    
    fsm_hv_percentages = []
    number_of_errors = 0
    
    for i in sb_range:
    
        if i in fsm_lbns:
    
            fsm_lbn = int(fsm_lbns[j])
    
            number_of_errors = fsm_hv_df[fsm_hv_df['LumiBlock Number'] == i].shape[0]
    
            fsm_hv_percentage = (number_of_chambers - number_of_errors) / number_of_chambers * 100
    
            fsm_hv_percentages.append(fsm_hv_percentage)
    
            j += 1
    
        else:
    
            fsm_hv_percentages.append(100)

    fsm_lbns = fsm_lv_df['LumiBlock Number'].unique()
    
    j = 0
    
    fsm_lv_percentages = []
    number_of_errors = 0
    
    for i in sb_range:
    
        if i in fsm_lbns:
    
            fsm_lbn = int(fsm_lbns[j])
    
            number_of_errors = fsm_lv_df[fsm_lv_df['LumiBlock Number'] == i].shape[0]
    
            fsm_lv_percentage = (number_of_chambers - number_of_errors) / number_of_chambers * 100
    
            fsm_lv_percentages.append(fsm_lv_percentage)
    
            j += 1
    
        else:
    
            fsm_lv_percentages.append(100)

    percentages_df = pd.DataFrame({'Stable Beam LumiBlock Number' : sb_range, 
                                   'JTAG DQ Percentage' : fsm_jtag_percentages, 
                                   'HV DQ Percentage' : fsm_hv_percentages, 
                                   'LV DQ Percentage' : fsm_lv_percentages})

    percentages = [(sb_range, percentages_df['JTAG DQ Percentage'].values.tolist(), 'JTAG DQ Percentage'), 
                   (sb_range, percentages_df['HV DQ Percentage'].values.tolist(), 'HV DQ Percentage'), 
                   (sb_range, percentages_df['LV DQ Percentage'].values.tolist(), 'LV DQ Percentage')]

    return percentages, percentages_df

def generate_temp_tsys(temp_sensors_df, chamber):

    sensors = np.sort(temp_sensors_df['Sensor'].unique())

    tsys = []
    bad_chambers = []
    bad_sensors = []
    temp_sum = 0
    temp_len = 0

    for sensor in sensors:
    
        tmp_df = temp_sensors_df[temp_sensors_df['Sensor'] == sensor]
    
        temp = tmp_df['value']
        dt = tmp_df['dt']
        l = 'Sensor ' + str(sensor)
        
        if len(temp[(temp < 1) | (temp > 200)]) != 0:

            bad_chambers.append(chamber)
            bad_sensors.append(sensor)  
            
        else:

            temp_sum += sum(temp)
            temp_len += len(temp)
    
            tsys.append((dt, temp, l))

    bad_sensors_df = pd.DataFrame({'Chamber' : bad_chambers, 'Faulty Sensor' : bad_sensors})

    avg_temp = temp_sum/temp_len

    for ts, ys, ls, in tsys:

        avg_temp
            
    return tsys, bad_sensors_df, avg_temp

def generate_tower_flag_durations_df(df, type, till_ts, till_dt):

    flags = df[df['value'] > 0]    
    unique_datapoints = flags['datapoint'].unique()
    
    partitions = []
    towers = []
    start_tss = []
    end_tss = []
    start_dts = []
    end_dts = []
    total_times = []
    total_tower_flags = []
    
    for i in range(len(unique_datapoints)):
    
        unique_datapoint = unique_datapoints[i]
    
        tmp_df = df[df['datapoint'] == unique_datapoint]
        
        tmp_str = tmp_df['datapoint'].values[0][18:28]
    
        partition = tmp_str[:2]
    
        tower = tmp_str[-2:]
    
        tower = tower.replace('.', '')
    
        change_df = tmp_df[tmp_df['value'].diff() != 0].reset_index()
        
        if change_df.shape[0] != 1:
    
            while change_df.iloc[0]['value'] == 0:
    
                change_df.drop(index = 0, inplace = True)
    
            i = 0

            pairs = int(round(change_df.shape[0]/2-.25))
    
            for _ in range(pairs):
    
                tmp_start_ts = change_df.iloc[i]['ts']
                tmp_end_ts = change_df.iloc[i+1]['ts']
                tmp_start_dt = change_df.iloc[i]['dt']
                tmp_end_dt = change_df.iloc[i+1]['dt']
    
                total_time = tmp_end_dt - tmp_start_dt
    
                partitions.append(partition)
                towers.append(tower)
                start_tss.append(tmp_start_ts)
                end_tss.append(tmp_end_ts)
                start_dts.append(tmp_start_dt)
                end_dts.append(tmp_end_dt)
                total_times.append(total_time)
                total_tower_flags.append(pairs)
    
                i += 2
    
            if change_df.shape[0]%2 != 0:
    
                tmp_start_ts = change_df.iloc[-1]['ts']
                tmp_end_ts = till_ts
                tmp_start_dt = change_df.iloc[-1]['dt']
                tmp_end_dt = till_dt
    
                total_time = tmp_end_dt - tmp_start_dt
        
                partitions.append(partition)
                towers.append(tower)
                start_tss.append(tmp_start_ts)
                end_tss.append(tmp_end_ts)
                start_dts.append(tmp_start_dt)
                end_dts.append(tmp_end_dt)
                total_times.append(total_time)
                total_tower_flags.append(pairs + 1)

    time_total = np.sum(total_times)

    return_df = pd.DataFrame({'Partition' : partitions, 
                              'Tower' : towers, 
                              'Start ts' : start_tss, 
                              'End ts' : end_tss, 
                              'Start dt' : start_dts, 
                              'End dt' : end_dts, 
                              f'Total {type} Time' : total_times,
                              'Total Flags for Tower': total_tower_flags})
    
    return_df[f'Total {type} Time'] = return_df[f'Total {type} Time'].apply(lambda td: str(td)[7:])

    return return_df, time_total

def generate_chamber_flag_durations_df(df, type, till_ts, till_dt):

    flags = df[df['value'] > 0]    
    unique_datapoints = flags['datapoint'].unique()
    
    chambers = []
    start_tss = []
    end_tss = []
    start_dts = []
    end_dts = []
    total_times = []
    total_chamber_flags = []
    
    for i in range(len(unique_datapoints)):
    
        unique_datapoint = unique_datapoints[i]

        # print(unique_datapoint)
    
        tmp_df = df[df['datapoint'] == unique_datapoint]

        # print(tmp_df)
        
        chamber = tmp_df['datapoint'].values[0][17:24]

        # print(tmp_str)
    
        change_df = tmp_df[tmp_df['value'].diff() != 0].reset_index()
        
        if change_df.shape[0] != 1:
    
            while change_df.iloc[0]['value'] == 0:
    
                change_df.drop(index = 0, inplace = True)
    
            i = 0

            pairs = int(round(change_df.shape[0]/2-.25))
    
            for _ in range(pairs):
    
                tmp_start_ts = change_df.iloc[i]['ts']
                tmp_end_ts = change_df.iloc[i+1]['ts']
                tmp_start_dt = change_df.iloc[i]['dt']
                tmp_end_dt = change_df.iloc[i+1]['dt']
    
                total_time = tmp_end_dt - tmp_start_dt
    
                chambers.append(chamber)
                start_tss.append(tmp_start_ts)
                end_tss.append(tmp_end_ts)
                start_dts.append(tmp_start_dt)
                end_dts.append(tmp_end_dt)
                total_times.append(total_time)
                total_chamber_flags.append(pairs)
    
                i += 2
    
            if change_df.shape[0]%2 != 0:
    
                tmp_start_ts = change_df.iloc[-1]['ts']
                tmp_end_ts = till_ts
                tmp_start_dt = change_df.iloc[-1]['dt']
                tmp_end_dt = till_dt
    
                total_time = tmp_end_dt - tmp_start_dt
        
                chambers.append(chamber)
                start_tss.append(tmp_start_ts)
                end_tss.append(tmp_end_ts)
                start_dts.append(tmp_start_dt)
                end_dts.append(tmp_end_dt)
                total_times.append(total_time)
                total_chamber_flags.append(pairs + 1)

    time_total = np.sum(total_times)

    return_df = pd.DataFrame({'Chamber' : chambers, 
                              'Start ts' : start_tss, 
                              'End ts' : end_tss, 
                              'Start dt' : start_dts, 
                              'End dt' : end_dts, 
                              f'Total {type} Time' : total_times,
                              'Total Flags for Chamber': total_chamber_flags})
    
    return_df[f'Total {type} Time'] = return_df[f'Total {type} Time'].apply(lambda td: str(td)[7:])

    return return_df, time_total

def generate_mezz_flag_durations_df(df, dropMezzNum_df, dropMezzMask_df, till_ts, till_dt):

    flags = df[df['value'] > 0]    
    unique_datapoints = flags['datapoint'].unique()
    
    chambers = []
    DroppedMezzNums = []
    DroppedMezzMasks = []
    start_tss = []
    end_tss = []
    start_dts = []
    end_dts = []
    total_times = []
    total_mezz_flags = []
    
    for i in range(len(unique_datapoints)):
    
        unique_datapoint = unique_datapoints[i]

        # print(unique_datapoint)
    
        tmp_df = df[df['datapoint'] == unique_datapoint]

        # print(tmp_df)
        
        chamber = tmp_df['datapoint'].values[0][17:24]
    
        change_df = tmp_df[tmp_df['value'].diff() != 0].reset_index()
        
        if change_df.shape[0] != 1:
    
            while change_df.iloc[0]['value'] == 0:
    
                change_df.drop(index = 0, inplace = True)
    
            j = 0

            pairs = int(round(change_df.shape[0]/2-.25))
    
            for _ in range(pairs):
    
                tmp_start_ts = change_df.iloc[j]['ts']
                tmp_end_ts = change_df.iloc[j+1]['ts']
                tmp_start_dt = change_df.iloc[j]['dt']
                tmp_end_dt = change_df.iloc[j+1]['dt']
    
                total_time = tmp_end_dt - tmp_start_dt

                try:
                    
                    DroppedMezzNum = dropMezzNum_df.iloc[i]['value']

                except IndexError:

                    DroppedMezzNum = 'Not Available'

                try:
                    
                    DroppedMezzMask = hex(dropMezzMask_df.iloc[i]['value'])[2:]

                except IndexError:

                    DroppedMezzMask = 'Not Available'

                chambers.append(chamber)
                DroppedMezzNums.append(DroppedMezzNum)
                DroppedMezzMasks.append(DroppedMezzMask)
                start_tss.append(tmp_start_ts)
                end_tss.append(tmp_end_ts)
                start_dts.append(tmp_start_dt)
                end_dts.append(tmp_end_dt)
                total_times.append(total_time)
                total_mezz_flags.append(pairs)
    
                j += 2
    
            if change_df.shape[0]%2 != 0:
    
                tmp_start_ts = change_df.iloc[-1]['ts']
                tmp_end_ts = till_ts
                tmp_start_dt = change_df.iloc[-1]['dt']
                tmp_end_dt = till_dt
    
                total_time = tmp_end_dt - tmp_start_dt
        
                try:
                    
                    DroppedMezzNum = dropMezzNum_df.iloc[i]['value']

                except IndexError:

                    DroppedMezzNum = 'Not Available'

                try:
                    
                    DroppedMezzMask = hex(dropMezzMask_df.iloc[i]['value'])[2:]

                except IndexError:

                    DroppedMezzMask = 'Not Available'
    
                chambers.append(chamber)
                DroppedMezzNums.append(DroppedMezzNum)
                DroppedMezzMasks.append(DroppedMezzMask)
                start_tss.append(tmp_start_ts)
                end_tss.append(tmp_end_ts)
                start_dts.append(tmp_start_dt)
                end_dts.append(tmp_end_dt)
                total_times.append(total_time)
                total_mezz_flags.append(pairs + 1)

    time_total = np.sum(total_times)

    return_df = pd.DataFrame({'Chamber' : chambers, 
                              'Mezz Number' : DroppedMezzNums,
                              'Mezz Mask' : DroppedMezzMasks, 
                              'Start ts' : start_tss, 
                              'End ts' : end_tss, 
                              'Start dt' : start_dts, 
                              'End dt' : end_dts, 
                              'Total Drop Time' : total_times,
                              'Total Flags For Mezz': total_mezz_flags})
    
    return_df['Total Drop Time'] = return_df['Total Drop Time'].apply(lambda td: str(td)[7:])

    return return_df, time_total

def generate_unplugged_flag_durations_df(df, pbeast_mapping_df):
    
    flags = df[df['value'] > 0]    
    unique_datapoints = flags['datapoint'].unique()
    unplugged_mapping = pbeast_mapping_df[pbeast_mapping_df['comment_'].str.contains('.* Unplugged')]
    
    chambers = []
    layers = []
    start_tss = []
    end_tss = []
    start_dts = []
    end_dts = []
    total_times = []
    total_chamber_flags = []
    
    for i in range(len(unique_datapoints)):
    
        unique_datapoint = unique_datapoints[i]
    
        tmp_df = df[df['datapoint'] == unique_datapoint]

        channel_mapping = unplugged_mapping[unplugged_mapping['element_name'].str.contains(tmp_df.iloc[0]['datapoint'])]['comment_']
                
        partition = channel_mapping.values[0][7:9]
        chamber = channel_mapping.values[0][10:-17]
        layer = channel_mapping.values[0][18:21]
        
        change_df = tmp_df[tmp_df['value'].diff() != 0].reset_index()
        
        if change_df.shape[0] != 1:
    
            while change_df.iloc[0]['value'] == 0:
    
                change_df.drop(index = 0, inplace = True)
    
            i = 0

            pairs = int(round(change_df.shape[0]/2-.25))
    
            for _ in range(pairs):
    
                tmp_start_ts = change_df.iloc[i]['ts']
                tmp_end_ts = change_df.iloc[i+1]['ts']
                tmp_start_dt = change_df.iloc[i]['dt']
                tmp_end_dt = change_df.iloc[i+1]['dt']
    
                total_time = tmp_end_dt - tmp_start_dt
    
                chambers.append(chamber)
                layers.append(layer)
                start_tss.append(tmp_start_ts)
                end_tss.append(tmp_end_ts)
                start_dts.append(tmp_start_dt)
                end_dts.append(tmp_end_dt)
                total_times.append(total_time)
                total_chamber_flags.append(pairs)
    
                i += 2
    
            if change_df.shape[0]%2 != 0:
    
                tmp_start_ts = change_df.iloc[-1]['ts']
                tmp_end_ts = till_ts
                tmp_start_dt = change_df.iloc[-1]['dt']
                tmp_end_dt = till_dt
    
                total_time = tmp_end_dt - tmp_start_dt
        
                chambers.append(chamber)
                layers.append(layer)
                start_tss.append(tmp_start_ts)
                end_tss.append(tmp_end_ts)
                start_dts.append(tmp_start_dt)
                end_dts.append(tmp_end_dt)
                total_times.append(total_time)
                total_chamber_flags.append(pairs + 1)

    time_total = np.sum(total_times)

    return_df = pd.DataFrame({'Chamber' : chambers,
                              'Layer' : layers,
                              'Start ts' : start_tss, 
                              'End ts' : end_tss, 
                              'Start dt' : start_dts, 
                              'End dt' : end_dts, 
                              'Total unplugged time' : total_times,
                              'Total Flags for Chamber': total_chamber_flags})
    
    return_df['Total unplugged time'] = return_df['Total unplugged time'].apply(lambda td: str(td)[7:])

    return return_df, time_total

############################# Chamber Colormap Plotting Functions ######################

def get_color_from_temp(temp, max_temp, min_temp):

    index = round( ( temp - min_temp ) / ( max_temp - min_temp ) * 14 )

    return index

def plot_ei(df, side, temp = False, max_temp = None, min_temp = None):

    colorscale = plotly.colors.sequential.Turbo

    fig = go.Figure()

    center = np.array([0, 0])
    radius_inner = 1.0
    radius_outer = 8.0
    num_sections = 16
    num_eta = 8
    angular_offset = np.pi / num_sections + np.pi

    def get_vertex_coords(radius, angle, offset = 0):
        x = center[0] + (radius + offset) * np.cos(angle)
        y = center[1] + (radius + offset) * np.sin(angle)
        return [x, y]

    def get_polygon_vertices(ring_index, segment_index, offset = 0):
        start_angle = - 2 * np.pi * (segment_index - 1) / num_sections - angular_offset
        end_angle = - 2 * np.pi * (segment_index) / num_sections - angular_offset
        
        inner_radius = radius_inner + ring_index
        outer_radius = radius_inner + ring_index + 1

        v1 = get_vertex_coords(inner_radius, start_angle, offset)
        v2 = get_vertex_coords(inner_radius, end_angle, offset)
        v3 = get_vertex_coords(outer_radius, end_angle, offset)
        v4 = get_vertex_coords(outer_radius, start_angle, offset)
        
        return [v1, v2, v3, v4]

    # Draw the polygons
    for ring_index in range(num_eta):
        
        for segment_index in range(num_sections):
            
            # Get the chamber name
            if ring_index == 3:

                if segment_index % 2 == 0:

                    chamber_name = f'EIL{ring_index+1}{side}0{segment_index+1}'

                    polygon_vertices = get_polygon_vertices(ring_index, segment_index)
            
                    # Calculate the polygon center
                    polygon_center_x = np.mean([v[0] for v in polygon_vertices])
                    polygon_center_y = np.mean([v[1] for v in polygon_vertices])

                    if temp == False:

                        tmp_df = df[df['chamber_name'].str.contains(chamber_name)]
                        list_of_errors = tmp_df['value'].values
                
                        # Choose the color based on whether the chamber is in fsm_df
                        
                        if len(tmp_df) > 0:
                            fillcolor = 'crimson'
                            text = f'{chamber_name}: {list_of_errors}'
                        else:
                            fillcolor = 'lime'
                            text = f'{chamber_name}: READY'
            
                    else:
            
                        tmp_df = df[df['Chamber'].str.contains(chamber_name)]
                
                        # Choose the color based on tempurature
                        
                        if len(tmp_df) > 0:
            
                            avg_temp = tmp_df.iloc[0]['Average Temperature'].round(2)
            
                            if avg_temp == np.nan:
            
                                fillcolor = 'red'
                                text = f'{chamber_name}: Faulty Chamber Sensors'
            
                            else:
                            
                                fillcolor = colorscale[get_color_from_temp(avg_temp, max_temp, min_temp)]
                                text = f'{chamber_name}: {avg_temp}'

                        else:
                            
                            fillcolor = 'white'
                            text = f'{chamber_name}: No Data'
        
                    # Create the polygon
                    x_vals = [v[0] for v in polygon_vertices] + [polygon_vertices[0][0]]
                    y_vals = [v[1] for v in polygon_vertices] + [polygon_vertices[0][1]]
        
                    fig.add_trace(go.Scatter(
                        x=x_vals, y=y_vals,
                        mode = 'lines',
                        fill='toself',
                        fillcolor=fillcolor,
                        line=dict(color='black'),
                        hoverinfo='skip'
                    ))

                    fig.add_trace(go.Scatter(
                        x=[polygon_center_x], y=[polygon_center_y],
                        marker_color = fillcolor,
                        marker_size = 1,
                        name=chamber_name,
                        zorder=2,
                        hoverinfo='text',
                        text=text,
                    ))
        
            elif ring_index > 5:

                if segment_index == 4:

                    if ring_index == 7:
                
                        chamber_name = f'EEL{ring_index+1-6}{side}0{segment_index+1}'
        
                        polygon_vertices = get_polygon_vertices(ring_index, segment_index, offset = -.5)
                
                        # Calculate the polygon center
                        polygon_center_x = np.mean([v[0] for v in polygon_vertices])
                        polygon_center_y = np.mean([v[1] for v in polygon_vertices])
            
                        # Choose the color based on whether the chamber is in fsm_df
                        if temp == False:

                            tmp_df = df[df['chamber_name'].str.contains(chamber_name)]
                            list_of_errors = tmp_df['value'].values
                    
                            # Choose the color based on whether the chamber is in fsm_df
                            
                            if len(tmp_df) > 0:
                                fillcolor = 'crimson'
                                text = f'{chamber_name}: {list_of_errors}'
                            else:
                                fillcolor = 'lime'
                                text = f'{chamber_name}: READY'
                
                        else:
                
                            tmp_df = df[df['Chamber'].str.contains(chamber_name)]
                    
                            # Choose the color based on tempurature
                            
                            if len(tmp_df) > 0:
                
                                avg_temp = tmp_df.iloc[0]['Average Temperature'].round(2)
                
                                if avg_temp == np.nan:
                
                                    fillcolor = 'red'
                                    text = f'{chamber_name}: Faulty Chamber Sensors'
                
                                else:
                                
                                    fillcolor = colorscale[get_color_from_temp(avg_temp, max_temp, min_temp)]
                                    text = f'{chamber_name}: {avg_temp}'

                            else:
                                
                                fillcolor = 'white'
                                text = f'{chamber_name}: No Data'
            
                        # Create the polygon
                        x_vals = [v[0] for v in polygon_vertices] + [polygon_vertices[0][0]]
                        y_vals = [v[1] for v in polygon_vertices] + [polygon_vertices[0][1]]
            
            
                        fig.add_trace(go.Scatter(
                            x=x_vals, y=y_vals,
                            mode = 'lines',
                            fill='toself',
                            fillcolor=fillcolor,
                            line=dict(color='black'),
                            hoverinfo='skip'
                        ))
                        fig.add_trace(go.Scatter(
                            x=[polygon_center_x], y=[polygon_center_y],
                            marker_color = fillcolor,
                            marker_size = 1,
                            name=chamber_name,
                            zorder=2,
                            hoverinfo='text',
                            text=text,
                        ))
                
                else:

                    if segment_index % 2 == 0:

                        chamber_name = f'EEL{ring_index+1-6}{side}0{segment_index+1}'

                    else:

                        chamber_name = f'EES{ring_index+1-6}{side}0{segment_index+1}'
                        
                    polygon_vertices = get_polygon_vertices(ring_index, segment_index)
            
                    # Calculate the polygon center
                    polygon_center_x = np.mean([v[0] for v in polygon_vertices])
                    polygon_center_y = np.mean([v[1] for v in polygon_vertices])
        
                    # Choose the color based on whether the chamber is in fsm_df
                    if temp == False:

                        tmp_df = df[df['chamber_name'].str.contains(chamber_name)]
                        list_of_errors = tmp_df['value'].values
                
                        # Choose the color based on whether the chamber is in fsm_df
                        
                        if len(tmp_df) > 0:
                            fillcolor = 'crimson'
                            text = f'{chamber_name}: {list_of_errors}'
                        else:
                            fillcolor = 'lime'
                            text = f'{chamber_name}: READY'
            
                    else:
            
                        tmp_df = df[df['Chamber'].str.contains(chamber_name)]
                
                        # Choose the color based on tempurature
                        
                        if len(tmp_df) > 0:
            
                            avg_temp = tmp_df.iloc[0]['Average Temperature'].round(2)
            
                            if avg_temp == np.nan:
            
                                fillcolor = 'red'
                                text = f'{chamber_name}: Faulty Chamber Sensors'
            
                            else:
                            
                                fillcolor = colorscale[get_color_from_temp(avg_temp, max_temp, min_temp)]
                                text = f'{chamber_name}: {avg_temp}'

                        else:
                            
                            fillcolor = 'white'
                            text = f'{chamber_name}: No Data'
        
                    # Create the polygon
                    x_vals = [v[0] for v in polygon_vertices] + [polygon_vertices[0][0]]
                    y_vals = [v[1] for v in polygon_vertices] + [polygon_vertices[0][1]]
        
                    fig.add_trace(go.Scatter(
                        x=x_vals, y=y_vals,
                        mode = 'lines',
                        fill='toself',
                        fillcolor=fillcolor,
                        line=dict(color='black'),
                        hoverinfo='skip'
                    ))

                    fig.add_trace(go.Scatter(
                        x=[polygon_center_x], y=[polygon_center_y],
                        marker_color = fillcolor,
                        marker_size = 1,
                        name=chamber_name,
                        zorder=2,
                        hoverinfo='text',
                        text=text,
                    ))

    fig.update_layout(
    title = {
        'text': f'EI{side}',  # Set the title text
        'x': 0.5,  # Center the title horizontally (x=0.5 is the center of the plot)
        'y': .99,
        'xanchor': 'center',  # Anchors the title in the center
        'font': {
            'size': 40,  # Increase the font size
                }
        },
    showlegend=False,
    xaxis=dict(scaleanchor='y', showgrid=False, zeroline=False, showticklabels=False),
    yaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
    height=450, width=450,
    margin=dict(l=0,r=0,b=0,t=50)
    )

    return fig

def plot_em(df, side, temp = False, max_temp = None, min_temp = None):

    colorscale = plotly.colors.sequential.Turbo
    
    fig = go.Figure()
    
    center = np.array([0, 0])
    radius_inner = 1.0
    radius_outer = 5.0
    num_sections = 16
    num_eta = 5
    angular_offset = np.pi / num_sections + np.pi

    def get_vertex_coords(radius, angle):
        x = center[0] + radius * np.cos(angle)
        y = center[1] + radius * np.sin(angle)
        return [x, y]

    def get_polygon_vertices(ring_index, segment_index):
        start_angle = - 2 * np.pi * (segment_index - 1) / num_sections - angular_offset
        end_angle = - 2 * np.pi * (segment_index) / num_sections - angular_offset
        
        inner_radius = radius_inner + ring_index
        outer_radius = radius_inner + ring_index + 1

        v1 = get_vertex_coords(inner_radius, start_angle)
        v2 = get_vertex_coords(inner_radius, end_angle)
        v3 = get_vertex_coords(outer_radius, end_angle)
        v4 = get_vertex_coords(outer_radius, start_angle)
        
        return [v1, v2, v3, v4]

    # Draw the polygons
    for ring_index in range(num_eta):
        
        for segment_index in range(num_sections):

            polygon_vertices = get_polygon_vertices(ring_index, segment_index)
            
            # Calculate the polygon center
            polygon_center_x = np.mean([v[0] for v in polygon_vertices])
            polygon_center_y = np.mean([v[1] for v in polygon_vertices])

            # Get the chamber name
            if segment_index < 10:

                if segment_index % 2 != 0:
                    
                    chamber_name = f'EMS{ring_index+1}{side}0{segment_index+1}'

                else:

                    chamber_name = f'EML{ring_index+1}{side}0{segment_index+1}'

            else:
                
                if segment_index % 2 != 0:
                    
                    chamber_name = f'EMS{ring_index+1}{side}{segment_index+1}'

                else:

                    chamber_name = f'EML{ring_index+1}{side}{segment_index+1}'
                    
            if temp == False:

                tmp_df = df[df['chamber_name'].str.contains(chamber_name)]
                list_of_errors = tmp_df['value'].values
        
                # Choose the color based on whether the chamber is in fsm_df
                
                if len(tmp_df) > 0:
                    fillcolor = 'crimson'
                    text = f'{chamber_name}: {list_of_errors}'
                else:
                    fillcolor = 'lime'
                    text = f'{chamber_name}: READY'
    
            else:
    
                tmp_df = df[df['Chamber'].str.contains(chamber_name)]
        
                # Choose the color based on tempurature
                
                if len(tmp_df) > 0:
    
                    avg_temp = tmp_df.iloc[0]['Average Temperature'].round(2)
    
                    if avg_temp == np.nan:
    
                        fillcolor = 'red'
                        text = f'{chamber_name}: Faulty Chamber Sensors'
    
                    else:
                    
                        fillcolor = colorscale[get_color_from_temp(avg_temp, max_temp, min_temp)]
                        text = f'{chamber_name}: {avg_temp}'

                else:
                            
                    fillcolor = 'white'
                    text = f'{chamber_name}: No Data'

            # Create the polygon
            x_vals = [v[0] for v in polygon_vertices] + [polygon_vertices[0][0]]
            y_vals = [v[1] for v in polygon_vertices] + [polygon_vertices[0][1]]

            fig.add_trace(go.Scatter(
                x=x_vals, y=y_vals,
                mode = 'lines',
                fill='toself',
                fillcolor=fillcolor,
                line=dict(color='black'),
                hoverinfo='skip'
            ))

            fig.add_trace(go.Scatter(
                x=[polygon_center_x], y=[polygon_center_y],
                marker_color = fillcolor,
                marker_size = 1,
                name=chamber_name,
                zorder=2,
                hoverinfo='text',
                text=text,
            ))

    # Update the layout
    fig.update_layout(
    title = {
        'text': f'EM{side}',  # Set the title text
        'x': 0.5,  # Center the title horizontally (x=0.5 is the center of the plot)
        'y': .99,
        'xanchor': 'center',  # Anchors the title in the center
        'font': {
            'size': 40,  # Increase the font size
                }
        },
    showlegend=False,
    xaxis=dict(scaleanchor='y', showgrid=False, zeroline=False, showticklabels=False),
    yaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
    height=450, width=450,
    margin=dict(l=0,r=0,b=0,t=50)
    )

    return fig

def plot_eo(df, side, temp = False, max_temp = None, min_temp = None):

    colorscale = plotly.colors.sequential.Turbo

    fig = go.Figure()

    center = np.array([0, 0]) 
    radius_inner = 1.0
    radius_outer = 6.0
    num_sections = 16
    num_eta = 6
    angular_offset = np.pi / num_sections + np.pi

    def get_vertex_coords(radius, angle):
        x = center[0] + radius * np.cos(angle)
        y = center[1] + radius * np.sin(angle)
        return [x, y]

    def get_polygon_vertices(ring_index, segment_index):
        start_angle = - 2 * np.pi * (segment_index - 1) / num_sections - angular_offset
        end_angle = - 2 * np.pi * (segment_index) / num_sections - angular_offset
        
        inner_radius = radius_inner + ring_index
        outer_radius = radius_inner + ring_index + 1

        v1 = get_vertex_coords(inner_radius, start_angle)
        v2 = get_vertex_coords(inner_radius, end_angle)
        v3 = get_vertex_coords(outer_radius, end_angle)
        v4 = get_vertex_coords(outer_radius, start_angle)
        
        return [v1, v2, v3, v4]

    # Draw the polygons
    for ring_index in range(num_eta):
        
        for segment_index in range(num_sections):
            
            polygon_vertices = get_polygon_vertices(ring_index, segment_index)
            
            # Calculate the polygon center
            polygon_center_x = np.mean([v[0] for v in polygon_vertices])
            polygon_center_y = np.mean([v[1] for v in polygon_vertices])

            # Get the chamber name
            if segment_index < 10:

                if segment_index % 2 != 0:
                    
                    chamber_name = f'EOS{ring_index+1}{side}0{segment_index+1}'

                else:

                    chamber_name = f'EOL{ring_index+1}{side}0{segment_index+1}'

            else:
                
                if segment_index % 2 != 0:
                    
                    chamber_name = f'EOS{ring_index+1}{side}{segment_index+1}'

                else:

                    chamber_name = f'EOL{ring_index+1}{side}{segment_index+1}'

            if temp == False:
    
                tmp_df = df[df['chamber_name'].str.contains(chamber_name)]
                list_of_errors = tmp_df['value'].values
        
                # Choose the color based on whether the chamber is in fsm_df
                
                if len(tmp_df) > 0:
                    fillcolor = 'crimson'
                    text = f'{chamber_name}: {list_of_errors}'
                else:
                    fillcolor = 'lime'
                    text = f'{chamber_name}: READY'
    
            else:
    
                tmp_df = df[df['Chamber'].str.contains(chamber_name)]
        
                # Choose the color based on tempurature
                
                if len(tmp_df) > 0:
    
                    avg_temp = tmp_df.iloc[0]['Average Temperature'].round(2)
    
                    if avg_temp == np.nan:
    
                        fillcolor = 'red'
                        text = f'{chamber_name}: Faulty Chamber Sensors'
    
                    else:
                    
                        fillcolor = colorscale[get_color_from_temp(avg_temp, max_temp, min_temp)]
                        text = f'{chamber_name}: {avg_temp}'

                else:
                            
                    fillcolor = 'white'
                    text = f'{chamber_name}: No Data'

            # Create the polygon
            x_vals = [v[0] for v in polygon_vertices] + [polygon_vertices[0][0]]
            y_vals = [v[1] for v in polygon_vertices] + [polygon_vertices[0][1]]

            fig.add_trace(go.Scatter(
                x=x_vals, y=y_vals,
                mode = 'lines',
                fill='toself',
                fillcolor=fillcolor,
                line=dict(color='black'),
                hoverinfo='skip'
            ))


            fig.add_trace(go.Scatter(
                x=[polygon_center_x], y=[polygon_center_y],
                marker_color = fillcolor,
                marker_size = 1,
                name=chamber_name,
                zorder=2,
                hoverinfo='text',
                text=text,
            ))

    # Update the layout
    fig.update_layout(
    title = {
        'text': f'EO{side}',  # Set the title text
        'x': 0.5,  # Center the title horizontally (x=0.5 is the center of the plot)
        'y': .99,
        'xanchor': 'center',  # Anchors the title in the center
        'font': {
            'size': 40,  # Increase the font size
                }
        },
    showlegend=False,
    xaxis=dict(scaleanchor='y', showgrid=False, zeroline=False, showticklabels=False),
    yaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
    height=450, width=450,
    margin=dict(l=0,r=0,b=0,t=50)
    )

    return fig

def plot_be_bi(xs_ys_df, df, temp = False, max_temp = None, min_temp = None):

    colorscale = plotly.colors.sequential.Turbo
    
    xs_list = []
    ys_list = []
    
    fig = go.Figure()
    
    box_size = 10
    
    for x0, y0, chamber_name in zip(xs_ys_df['x0'].astype(int), xs_ys_df['y0'].astype(int), xs_ys_df['chamber']):
        
        if temp == False:

            tmp_df = df[df['chamber_name'].str.contains(chamber_name)]
            list_of_errors = tmp_df['value'].values
    
            # Choose the color based on whether the chamber is in fsm_df
            
            if len(tmp_df) > 0:
                fillcolor = 'crimson'
                text = f'{chamber_name}: {list_of_errors}'
            else:
                fillcolor = 'lime'
                text = f'{chamber_name}: READY'

        else:

            tmp_df = df[df['Chamber'].str.contains(chamber_name)]
    
            # Choose the color based on tempurature
            
            if len(tmp_df) > 0:

                avg_temp = tmp_df.iloc[0]['Average Temperature'].round(2)

                if avg_temp == np.nan:

                    fillcolor = 'red'
                    text = f'{chamber_name}: Faulty Chamber Sensors'

                else:
                
                    fillcolor = colorscale[get_color_from_temp(avg_temp, max_temp, min_temp)]
                    text = f'{chamber_name}: {avg_temp}'

            else:
                            
                fillcolor = 'white'
                text = f'{chamber_name}: No Data'
    
        xs = [x0, x0+box_size, x0+box_size, x0, x0]
        ys = [y0, y0, y0 + box_size, y0 + box_size, y0]
        xs_list.append(y0)
        ys_list.append(-x0 + 300)
    
        fig.add_trace(go.Scatter(
            x=xs, y=ys,
            mode = 'lines',
            fill='toself',
            fillcolor=fillcolor,
            line=dict(color='black'),
            hoverinfo='skip'
        ))

        fig.add_trace(go.Scatter(
            x=[x0+5], y=[y0+5],
            marker_color = fillcolor,
            marker_size = 1,
            name=chamber_name,
            zorder=2,
            hoverinfo='text',
            text=text,
        ))
    
    fig.update_layout(
    title = {
        'text': 'BE/BI',  # Set the title text
        'x': 0.5,  # Center the title horizontally (x=0.5 is the center of the plot)
        'y': .99,
        'xanchor': 'center',  # Anchors the title in the center
        'font': {
            'size': 40,  # Increase the font size
                }
        },
    showlegend=False,
    xaxis=dict(scaleanchor='y', showgrid=False, zeroline=False, showticklabels=False),
    yaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
    height=450, width=450,
    margin=dict(l=0,r=0,b=0,t=50)
    )
        
    return fig

def plot_bm(xs_ys_df, df, temp = False, max_temp = None, min_temp = None):

    colorscale = plotly.colors.sequential.Turbo
    
    xs_list = []
    ys_list = []
    
    fig = go.Figure()
    
    box_size = 10
    
    for x0, y0, chamber_name in zip(xs_ys_df['x0'].astype(int), xs_ys_df['y0'].astype(int), xs_ys_df['chamber']):
            
        if temp == False:

            tmp_df = df[df['chamber_name'].str.contains(chamber_name)]
            list_of_errors = tmp_df['value'].values
    
            # Choose the color based on whether the chamber is in fsm_df
            
            if len(tmp_df) > 0:
                fillcolor = 'crimson'
                text = f'{chamber_name}: {list_of_errors}'
            else:
                fillcolor = 'lime'
                text = f'{chamber_name}: READY'

        else:

            tmp_df = df[df['Chamber'].str.contains(chamber_name)]
    
            # Choose the color based on tempurature
            
            if len(tmp_df) > 0:

                avg_temp = tmp_df.iloc[0]['Average Temperature'].round(2)

                if avg_temp == np.nan:

                    fillcolor = 'red'
                    text = f'{chamber_name}: Faulty Chamber Sensors'

                else:
                
                    fillcolor = colorscale[get_color_from_temp(avg_temp, max_temp, min_temp)]
                    text = f'{chamber_name}: {avg_temp}'

            else:
                            
                fillcolor = 'white'
                text = f'{chamber_name}: No Data'
    
        xs = [x0, x0+box_size, x0+box_size, x0, x0]
        ys = [y0, y0, y0 + box_size, y0 + box_size, y0]
    
        fig.add_trace(go.Scatter(
            x=xs, y=ys,
            mode = 'lines',
            fill='toself',
            fillcolor=fillcolor,
            line=dict(color='black'),
            hoverinfo='skip'
        ))

        fig.add_trace(go.Scatter(
            x=[x0+5], y=[y0+5],
            marker_color = fillcolor,
            marker_size = 1,
            name=chamber_name,
            zorder=2,
            hoverinfo='text',
            text=text,
        ))
    
    fig.update_layout(
    title = {
        'text': 'BM',  # Set the title text
        'x': 0.5,  # Center the title horizontally (x=0.5 is the center of the plot)
        'y': .99,
        'xanchor': 'center',  # Anchors the title in the center
        'font': {
            'size': 40,  # Increase the font size
                }
        },
    showlegend=False,
    xaxis=dict(range=[-90, 90], scaleanchor='y', showgrid=False, zeroline=False, showticklabels=False),
    yaxis=dict(range=[-142, 138], showgrid=False, zeroline=False, showticklabels=False),
    height=450, width=450,
    margin=dict(l=0,r=0,b=0,t=50)
    )
        
    return fig

def plot_bo(xs_ys_df, df, temp = False, max_temp = None, min_temp = None):

    colorscale = plotly.colors.sequential.Turbo
    
    xs_list = []
    ys_list = []
    
    fig = go.Figure()
    
    box_size = 10
    
    for x0, y0, chamber_name in zip(xs_ys_df['x0'].astype(int), xs_ys_df['y0'].astype(int), xs_ys_df['chamber']):

        if temp == False:

            tmp_df = df[df['chamber_name'].str.contains(chamber_name)]
            list_of_errors = tmp_df['value'].values
    
            # Choose the color based on whether the chamber is in fsm_df
            if len(tmp_df) > 0:
                fillcolor = 'crimson'
                text = f'{chamber_name}: {list_of_errors}'
            else:
                fillcolor = 'lime'
                text = f'{chamber_name}: READY'

        else:

            tmp_df = df[df['Chamber'].str.contains(chamber_name)]
    
            # Choose the color based on tempurature
            
            if len(tmp_df) > 0:

                avg_temp = tmp_df.iloc[0]['Average Temperature'].round(2)

                if avg_temp == np.nan:

                    fillcolor = 'red'
                    text = f'{chamber_name}: Faulty Chamber Sensors'

                else:
                
                    fillcolor = colorscale[get_color_from_temp(avg_temp, max_temp, min_temp)]
                    text = f'{chamber_name}: {avg_temp}'

            else:
                        
                fillcolor = 'white'
                text = f'{chamber_name}: No Data'
    
        xs = [x0, x0+box_size, x0+box_size, x0, x0]
        ys = [y0, y0, y0 + box_size, y0 + box_size, y0]
    
        fig.add_trace(go.Scatter(
            x=xs, y=ys,
            mode = 'lines',
            fill='toself',
            fillcolor=fillcolor,
            line=dict(color='black'),
            hoverinfo='skip'
        ))

        fig.add_trace(go.Scatter(
            x=[x0+5], y=[y0+5],
            marker_color = fillcolor,
            marker_size = 1,
            name=chamber_name,
            zorder=2,
            hoverinfo='text',
            text=text,
        ))
    
    fig.update_layout(
    title = {
        'text': 'BO',  # Set the title text
        'x': 0.5,  # Center the title horizontally (x=0.5 is the center of the plot)
        'y': .99,
        'xanchor': 'center',  # Anchors the title in the center
        'font': {
            'size': 40,  # Increase the font size
                }
        },
    showlegend=False,
    xaxis=dict(range=[-110, 110], scaleanchor='y', showgrid=False, zeroline=False, showticklabels=False),
    yaxis=dict(range=[-130, 138], showgrid=False, zeroline=False, showticklabels=False),
    height=450, width=450,
    margin=dict(l=0,r=0,b=0,t=50)
    )
        
    return fig

###############################################################################