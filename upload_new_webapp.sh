start_time=$(date +%s)

oc sso-login --server https://api.paas.okd.cern.ch
cd $HOME
rsync -av --exclude=".*" /eos/user/m/mdtoffline/mdt_dcs_offline/streamlit/mdt-dcs-viewer .
cd mdt-dcs-viewer
docker build -t gitlab-registry.cern.ch/josteele/mdt_dcs_offline_webapp .
docker push gitlab-registry.cern.ch/josteele/mdt_dcs_offline_webapp
cd ..
rm -r mdt-dcs-viewer
oc import-image mdt --all

finish_time=$(date +%s)
elapsed_time=$((finish_time  - start_time))
((sec=elapsed_time%60, elapsed_time/=60, min=elapsed_time%60, hrs=elapsed_time/60))
timestamp=$(printf "Total time taken - %d hours, %d minutes, and %d seconds.\n" $hrs $min $sec)
echo -e $timestamp
date