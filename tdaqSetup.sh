#!/bin/bash

# Ensure /cvmfs is mounted
while [ ! -d /cvmfs ]; do
  echo "Waiting for /cvmfs to be mounted..."
  sleep 2
done

# Source the required script from /cvmfs
CMTCONFIG=x86_64-el9-gcc13-opt 
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-11-02-01
echo "done setup of TDAQ"

# Run Entrypoint using venv python and venv streamlit
/app/myvenv/bin/python /app/myvenv/bin/streamlit run MDT_DCS_Viewer.py --server.port=8501 --server.address=0.0.0.0 
