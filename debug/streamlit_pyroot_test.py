import streamlit as st
import pandas as pd
import numpy as np
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

df = pd.DataFrame(np.random.default_rng(10).random((10,5)), columns=('col #' + str(i) for i in range(5)))

def configure_page() -> None:
    st.set_page_config(page_title="PyROOT Test", layout="wide")


def configure_overview() -> None:
    st.markdown("## Overview")
    st.markdown(
        "Lets plot a histogram!"
    )
    st.markdown(
        "I hope this works.."
    )

def data_frame() -> None:
    st.write(df)


# def configure_available_algo_heuristics() -> None:
#     st.markdown("## Algorithms and Heuristics")
#     st.markdown("The following algorithms and heuristics are available:")

#     with st.expander("Algorithms and Heuristics"):
#         left, right = st.columns(2)
#         left.markdown("### Algorithms")
#         for algorithm in ALGORITHMS:
#             left.markdown(f"- {algorithm.__name__}")

#         right.markdown("### Heuristics")
#         for heuristic in HEURISTICS:
#             right.markdown(f"- {heuristic.__name__.title()}")


def create_plot() -> Figure:
    fig = plt.figure(figsize=(4, 6), dpi = 50)
    plt.plot(range(10), df['col #1'])
    plt.show()
    return fig


def main() -> None:


    configure_page()
    configure_overview()
    data_frame()


    fig = create_plot()
    st.pyplot(fig)


if __name__ == "__main__":
    main()