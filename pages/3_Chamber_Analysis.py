######################################### Import Libaries #########################################

import streamlit as st
import pandas as pd
import numpy as np
import re
import gc
from datetime import datetime as dt
from datetime import timedelta
import os, stat
import requests

from functions import *

import plotly.graph_objects as go
from beauty import Beauty
import libpbeastpy

########################################### Initialize ############################################

# setup pbeast python API

db = libpbeastpy.ServerProxy('http://pc-tbed-bst-prod:8080')    # http://pc-tbed-bst-03:8080 is down
beauty = Beauty('http://pc-tbed-bst-prod:8080')

# setup streamlit

st.set_page_config(layout="wide", 
                   page_title="MDT DCS Data Viewer",
                   page_icon="📊")

st.write("""
# MDT DCS Data Viewer - Chamber Selection
""")

## Load Session State Variables from MDT_DCS_Viewer Home Page (streamlit-saved variables for each user session between pages when navigating to the webapp)

save_path = '/eos/user/m/mdtoffline/mdt_dcs_offline/streamlit/mdt-dcs-saved-run-analysis/'

if 'csv_path' not in st.session_state:
    
    st.session_state.csv_path = 'mdt_dcs_offline/streamlit/mdt-dcs-saved-run-analysis'

if 'pbeast_mapping_df' not in st.session_state:

    st.session_state.pbeast_mapping_df =  pd.read_csv('mappings/Pbeast_mapping.csv').drop(columns = 'Unnamed: 0')
    
if 'rn_sb_df' not in st.session_state:

    try:

        st.session_state.rn_sb_df = generate_rn_sb_df(dt(2024,4,1,0,0), dt.now())
        st.session_state.rn_sb_df.to_parquet(save_path + 'stable_beam_runs_df' + '.parquet')

    except RuntimeError:

        try:

            st.session_state.rn_sb_df = pd.read_parquet(save_path + 'stable_beam_runs_df' + '.parquet')

        except FileNotFoundError:

            st.write(f'## P-BEAST server currently unavailable and stable_beam_runs_df.parquet cannot be found in save directory: {save_path}')
    
if 'chamber_list' not in st.session_state:
    
    st.session_state.chamber_list = pd.read_csv('mappings/chamb_partition_list.csv')

if 'run_num' not in st.session_state:

    st.session_state.run_num = st.session_state.rn_sb_df['Stable Beam Run Number'].values.tolist()[0]

if 'run_num_index' not in st.session_state:

    st.session_state.run_num_index = st.session_state.rn_sb_df['Stable Beam Run Number'][st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num].index.tolist()[0]

if 'disabled' not in st.session_state:

    st.session_state.disabled = False

if 'chamber' not in st.session_state:

    st.session_state.chamber = st.session_state.chamber_list.iloc[0]['Chamber']

if 'chamber_index' not in st.session_state:

    st.session_state.chamber_index = st.session_state.chamber_list['Chamber'][st.session_state.chamber_list['Chamber'] == st.session_state.chamber].index.tolist()[0]

try:
    
    st.session_state.chamber_index = st.session_state.chamber_list['Chamber'][st.session_state.chamber_list['Chamber'] == st.session_state.chamber].index.tolist()[0]

except:

    st.write('Chamber selected not found in st.session_state.chamber_list')

## Sidebar

col1, col2 = st.sidebar.columns(2)

butt_next = col1.button('Previous Run', type = 'primary')

butt_prev = col2.button('Next Run', type = 'primary')

if butt_next == True:

    if st.session_state.run_num_index + 2 < st.session_state.rn_sb_df.shape[0]:

        st.session_state.run_num_index += 1
        
    else:

        st.session_state.run_num_index = 0
        
    butt_next = False

if butt_prev == True:

    st.session_state.run_num_index -= 1
    butt_prev = False

st.session_state.run_num = st.sidebar.selectbox(
    "Select a Run Number:",
    st.session_state.rn_sb_df['Stable Beam Run Number'],
    index = st.session_state.run_num_index
)

st.session_state.run_num_index = st.session_state.rn_sb_df['Stable Beam Run Number'][st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num].index.tolist()[0]

st.session_state.chamber = st.sidebar.selectbox(
"Select a Chamber:",
st.session_state.chamber_list['Chamber'],
index = st.session_state.chamber_index
)

st.sidebar.checkbox("Use custom date range", value = st.session_state.disabled, key="disabled")

st.session_state.ts_start_input = st.sidebar.text_input(
    "Select a start time (eg. 2024-06-15 04:22:00)",
    '2024-07-01 00:00:00',
    disabled = not st.session_state.disabled
)

st.session_state.ts_end_input = st.sidebar.text_input(
    "Select an end time (eg. 2024-06-30 04:22:00)",
    '2024-07-03 00:00:00',
    disabled = not st.session_state.disabled
)

if st.session_state.disabled == False:

    path = save_path + str(st.session_state.run_num)

    isExist = os.path.exists(path)
    
    if(not isExist):
        
        os.mkdir(path)
        os.mkdir(path + "/images")
        os.mkdir(path + "/csv")
        os.mkdir(path + "/chambers")
        os.chmod(path, stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/images", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/csv", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/chambers", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)

    chamber_path = path + "/chambers/" + st.session_state.chamber

    isExist = os.path.exists(chamber_path)
    
    if(not isExist):
        
        os.mkdir(chamber_path)
        os.chmod(chamber_path, stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)

    chamber_path = chamber_path + "/"

else:

    path = save_path + f"{since_dt}_{till_dt}"

    isExist = os.path.exists(path)
    
    if(not isExist):
        
        os.mkdir(path)
        os.mkdir(path + "/images")
        os.mkdir(path + "/csv")
        os.mkdir(path + "/chambers")
        os.chmod(path, stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/images", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/csv", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/chambers", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)

    chamber_path = path + "/chambers/" + st.session_state.chamber

    isExist = os.path.exists(chamber_path)
    
    if(not isExist):
        
        os.mkdir(chamber_path)
        os.chmod(chamber_path, stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)

    chamber_path = chamber_path + "/"

if st.session_state.disabled == True:
    
    ts_start_dt = st.session_state.ts_start_input
    ts_end_dt = st.session_state.ts_end_input
    
    fmt = '%Y-%m-%d %H:%M:%S'                                       # For user input in the sidebar

    since_dt = dt.strptime(ts_start_dt, fmt)
    till_dt = dt.strptime(ts_end_dt, fmt)

    since_ts = int((pd.to_datetime(ts_start_dt)).timestamp()*1e6)       # Convert these to timestamps, convert to us, convert to int
    till_ts = int((pd.to_datetime(ts_end_dt)).timestamp()*1e6)
    
else:
    ts_start_dt = st.session_state.rn_sb_df[st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num]['Run Start dt'].values[0]
    ts_end_dt = st.session_state.rn_sb_df[st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num]['Run End dt'].values[0]

    since_ts = int((pd.to_datetime(ts_start_dt)).timestamp()*1e6)
    till_ts = int((pd.to_datetime(ts_end_dt)).timestamp()*1e6)

# Convert datetime to timestamp

since_dt = pd.Timestamp.fromtimestamp((since_ts-7200000000)/1e6).tz_localize('UTC').tz_convert('Europe/Zurich')
till_dt = pd.Timestamp.fromtimestamp((till_ts-7200000000)/1e6).tz_localize('UTC').tz_convert('Europe/Zurich')

## Header

if st.session_state.disabled == False:
                            
    st.write(f"## Run {st.session_state.run_num}")

else:

    st.write(f"## {since_dt} - {till_dt}")

st.write(f'## Chamber {st.session_state.chamber}')

col1, col2 = st.columns(2)

with col1:

    try:
    
        comment = f'{st.session_state.chamber} ML1 HV Imon'
        query = st.session_state.pbeast_mapping_df[st.session_state.pbeast_mapping_df['comment_'].str.contains(comment)]['element_name'].values[0]
    
        hltmon = beauty.timeseries(since_dt.tz_convert(None), 
                                   till_dt.tz_convert(None), 
                                   'DCS', 
                                   'ATLAS_PVSSMDT', 
                                   'value-number', 
                                   query, 
                                   None, 
                                   False)[0]
        
        lumi = beauty.timeseries(since_dt.tz_convert(None), 
                                 till_dt.tz_convert(None), 
                                 'DCS', 
                                 'ATLAS_PVSSDCS', 
                                 'value-number', 
                                 'ATLGCSLUMI:ATLAS_PREFERRED_LBAv_PHYS_CalibLumi.value', 
                                 None, 
                                 False)[0]
    
        hv_lumi = hltmon.correlate(lumi)
    
        st.plotly_chart(lumi_plotly(hv_lumi, 
                                    query, 
                                    'ATLGCSLUMI:ATLAS_PREFERRED_LBAv_PHYS_CalibLumi.value',
                                    1), 
                        width = 500, 
                        height = 500)
    
        pd.DataFrame(hv_lumi).to_parquet(chamber_path + 'hv_lumi' + '.parquet')
        
        del hltmon
        del lumi
        gc.collect()

    except:

        st.write("### No Lumi-HV ML1 correlation found for this time range!")

    try:

        comment = f'{st.session_state.chamber} ML2 HV Imon'
        query = st.session_state.pbeast_mapping_df[st.session_state.pbeast_mapping_df['comment_'].str.contains(comment)]['element_name'].values[0]

        hltmon = beauty.timeseries(since_dt.tz_convert(None), 
                                   till_dt.tz_convert(None), 
                                   'DCS', 
                                   'ATLAS_PVSSMDT', 
                                   'value-number', 
                                   query, 
                                   None, 
                                   False)[0]
        
        lumi = beauty.timeseries(since_dt.tz_convert(None), 
                                 till_dt.tz_convert(None), 
                                 'DCS', 
                                 'ATLAS_PVSSDCS', 
                                 'value-number', 
                                 'ATLGCSLUMI:ATLAS_PREFERRED_LBAv_PHYS_CalibLumi.value', 
                                 None, 
                                 False)[0]
    
        hv_lumi = hltmon.correlate(lumi)
    
        st.plotly_chart(lumi_plotly(hv_lumi, 
                                    query, 
                                    'ATLGCSLUMI:ATLAS_PREFERRED_LBAv_PHYS_CalibLumi.value',
                                    2), 
                        width = 500, 
                        height = 500)
    
        pd.DataFrame(hv_lumi).to_parquet(chamber_path + 'hv_lumi' + '.parquet')
        
        del hltmon
        del lumi
        gc.collect()

    except:

        st.write("### No Lumi-HV ML2 correlation found for this time range!")

    try:
            
        condition = f'{st.session_state.chamber} .* LV Imon'
        queries = st.session_state.pbeast_mapping_df[st.session_state.pbeast_mapping_df['comment_'].str.contains(condition)]['element_name']
    
        tsys = []
    
        for query in queries:
        
            tmp_df = query_lib_data(since_ts, till_ts, query, 'MDT')
        
            if tmp_df.shape[0] != 0:
                      
                l = st.session_state.pbeast_mapping_df[st.session_state.pbeast_mapping_df['element_name'].str.contains(query)]['comment_'].values[0][6:]
                t = tmp_df['dt']
                y = tmp_df['value']
        
                tsys.append((t, y, l))
    
        title = f"LV Current <br><sup>{'<br>'.join(queries.values.tolist()) } </sup>"
        
        st.plotly_chart(time_plotly(tsys, 
                                    since_dt, 
                                    till_dt, 
                                    title, 
                                    'Current (uA)',  
                                    True),
                       width =600, height = 600)

        lv_imon_mean = y.mean()
    
        del tsys
        gc.collect()

    except:

        st.write("### No LV Current data found for this time range!")
    
    try:

        mdt_chamber_VCC_df = db.get_data('DCS', 
                                         'ATLAS_PVSSMDT', 
                                         'value-number', 
                                         f'.*{st.session_state.chamber}.*.Vcc', 
                                         True, 
                                         since_ts, 
                                         till_ts, 
                                         0, 
                                         True)
    
        mdt_chamber_VCC = queryDataToDataFrame(mdt_chamber_VCC_df)
        mdt_chamber_VCC.sort_values(by=['ts'], inplace=True)

        mdt_chamber_VCC.to_parquet(chamber_path + 'mdt_chamber_VCC' + '.parquet')
        
        t = pd.to_datetime(mdt_chamber_VCC['ts'], unit = 'us', utc = True).dt.tz_convert('Europe/Zurich')
        y = mdt_chamber_VCC['value'].astype('float64')
        vcc_mean = y.mean()

        title = f'VCC <br><sup>.*{st.session_state.chamber}.*.Vcc</sup>'
        
        st.plotly_chart(time_plotly([(t, y, f'{st.session_state.chamber} VCC')], 
                            since_dt, till_dt, 
                            title, 
                            'Voltage (V)', 
                            True))
        
        del mdt_chamber_VCC_df
        del mdt_chamber_VCC
        gc.collect()
    
    except:
    
        st.write("### No VCC data found for this chamber!")

with col2:

    try:
            
        condition = f'{st.session_state.chamber} .* HV Imon'
        queries = st.session_state.pbeast_mapping_df[st.session_state.pbeast_mapping_df['comment_'].str.contains(condition)]['element_name']
    
        tsys = []
    
        for query in queries:
        
            tmp_df = query_lib_data(since_ts, till_ts, query, 'MDT')
        
            if tmp_df.shape[0] != 0:
                      
                l = st.session_state.pbeast_mapping_df[st.session_state.pbeast_mapping_df['element_name'].str.contains(query)]['comment_'].values[0][-11:-8]
                t = tmp_df['dt']
                y = tmp_df['value']
                hv_imon_mean = y.mean()
        
                tsys.append((t, y, l))
    
        title = f"HV Current <br><sup>{'<br>'.join(queries.values.tolist()) } </sup>"
        
        st.plotly_chart(time_plotly(tsys, 
                                    since_dt, 
                                    till_dt, 
                                    title, 
                                    'Current (uA)',  
                                    True,
                                    ybound_scale=.4),
                       width =600, height = 600)
    
        del tsys
        gc.collect()

    except:

        st.write("### No HV Current data found for this time range!")

    try:
            
        condition = f'{st.session_state.chamber} .* HV Vmon'
        queries = st.session_state.pbeast_mapping_df[st.session_state.pbeast_mapping_df['comment_'].str.contains(condition)]['element_name']
    
        tsys = []
    
        for query in queries:
        
            tmp_df = query_lib_data(since_ts, till_ts, query, 'MDT')
        
            if tmp_df.shape[0] != 0:
                      
                l = st.session_state.pbeast_mapping_df[st.session_state.pbeast_mapping_df['element_name'].str.contains(query)]['comment_'].values[0][-11:-8]
                t = tmp_df['dt']
                y = tmp_df['value']
                hv_vmon_mean = y.mean()
        
                tsys.append((t, y, l))
    
        title = f"HV Voltage <br><sup>{'<br>'.join(queries.values.tolist()) } </sup>"
        
        st.plotly_chart(time_plotly(tsys, 
                                    since_dt, 
                                    till_dt, 
                                    title, 
                                    'Voltage (V)',  
                                    True),
                       width =600, height = 600)
    
        del tsys
        gc.collect()

    except:

        st.write("### No HV Voltage data found for this time range!")

    try:
            
        condition = f'{st.session_state.chamber} .* LV Vmon'
        queries = st.session_state.pbeast_mapping_df[st.session_state.pbeast_mapping_df['comment_'].str.contains(condition)]['element_name']
    
        tsys = []
    
        for query in queries:
        
            tmp_df = query_lib_data(since_ts, till_ts, query, 'MDT')
        
            if tmp_df.shape[0] != 0:
                      
                l = st.session_state.pbeast_mapping_df[st.session_state.pbeast_mapping_df['element_name'].str.contains(query)]['comment_'].values[0][6:]
                t = tmp_df['dt']
                y = tmp_df['value']
                lv_vmon_mean = y.mean()
    
                tsys.append((t, y, l))
    
        title = f"LV Voltage <br><sup>{'<br>'.join(queries.values.tolist()) } </sup>"
        
        st.plotly_chart(time_plotly(tsys, 
                                    since_dt, 
                                    till_dt, 
                                    title, 
                                    'Voltage (V)',  
                                    True),
                       width =600, height = 600)
    
        del tsys
        gc.collect()

    except:

        st.write("### No LV Voltage data found for this time range!")
    
    try:
        
        temp_sensors_df = db.get_data('DCS', 
                                      'ATLAS_PVSSMDT', 
                                      'value-number', 
                                      f'.*{st.session_state.chamber}.*.ntc.*', 
                                      True, 
                                      since_ts, 
                                      till_ts, 
                                      0, 
                                      True)
        
        temp_sensors_df = queryDataToDataFrame(temp_sensors_df)
        temp_sensors_df.sort_values(by=['ts'], inplace=True)
        temp_sensors_df['dt'] = pd.to_datetime(temp_sensors_df['ts'], unit = 'us', utc = True).dt.tz_convert('Europe/Zurich')
        temp_sensors_df['value'] = temp_sensors_df['value'].astype('float64')
        temp_sensors_df['Sensor'] = temp_sensors_df['datapoint'].apply(lambda x : x.split('.')[-2]).apply(lambda x : list(x)[-2:]).apply(lambda x : int(''.join(x)))
    
        temp_sensors_df.to_parquet(chamber_path + 'temp_sensors_df' + '.parquet')
    
        tsys, bad_sensors_df, avg_temp = generate_temp_tsys(temp_sensors_df, st.session_state.chamber)
            
        st.plotly_chart(time_plotly(tsys, 
                                    since_dt, 
                                    till_dt, 
                                    f'Temperature Sensors',
                                    'deg (C)', 
                                    True, 
                                    'Date (CEST)', 
                                    avg = avg_temp))
    
        if bad_sensors_df.shape[0] > 0:
        
            st.write(f'Faulty Temperature Sensors for Chamber {st.session_state.chamber}:')
            st.dataframe(bad_sensors_df, use_container_width = True)
    
        else:
    
            st.write(f'All Temperature Sensors functioning for Chamber {st.session_state.chamber}!')
    
        
        del temp_sensors_df
        gc.collect()

    except:
    
        st.write("### No temperature data found for this time range!")
    
avg_df = pd.DataFrame({'Temperature' : avg_temp, 'LV Current' : lv_imon_mean, 'LV Voltage' : lv_vmon_mean, 'HV Current' : hv_imon_mean, 'HV Voltage' : hv_vmon_mean, 'VCC' : vcc_mean}, index = ['Average'])

avg_df = (avg_df.style
        .set_table_styles([{'selector': 'th', 'props': [('font-size', '40pt'),
                                                        ("text-align", "center")]}])
        .set_properties(**{'font-size': '40pt'}))

st.dataframe(avg_df, use_container_width = True)

