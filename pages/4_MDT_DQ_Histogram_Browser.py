######################################### Import Libaries #########################################

import streamlit as st
import pandas as pd
import numpy as np
import re
import gc
from datetime import datetime as dt
from datetime import timedelta
import os, stat
import requests

from functions import *

import plotly.graph_objects as go
from beauty import Beauty
import libpbeastpy

########################################### Initialize ############################################

# setup pbeast python API

db = libpbeastpy.ServerProxy('http://pc-tbed-bst-prod:8080')    # http://pc-tbed-bst-03:8080 is down
beauty = Beauty('http://pc-tbed-bst-prod:8080')

# setup streamlit

st.set_page_config(layout="wide", 
                   page_title="MDT DCS Data Viewer",
                   page_icon="📊")

st.write("""
# MDT DQ Histogram Browser
""")

# st.cache_data.clear()

## Load Session State Variables from MDT_DCS_Viewer Home Page (streamlit-saved variables for each user session between pages when navigating to the webapp)

save_path = '/eos/user/m/mdtoffline/mdt_dcs_offline/streamlit/mdt-dcs-saved-run-analysis/'

if 'csv_path' not in st.session_state:
    
    st.session_state.csv_path = 'mdt_dcs_offline/streamlit/mdt-dcs-saved-run-analysis'
    
if 'rn_sb_df' not in st.session_state:

    try:

        st.session_state.rn_sb_df = generate_rn_sb_df(dt(2024,4,1,0,0), dt.now())
        st.session_state.rn_sb_df.to_parquet(save_path + 'stable_beam_runs_df' + '.parquet')

    except RuntimeError:

        try:

            st.session_state.rn_sb_df = pd.read_parquet(save_path + 'stable_beam_runs_df' + '.parquet')

        except FileNotFoundError:

            st.write(f'## P-BEAST server currently unavailable and stable_beam_runs_df.parquet cannot be found in save directory: {save_path}')

if 'chamber_list' not in st.session_state:
    
    st.session_state.chamber_list = pd.read_csv('mappings/chamb_partition_list.csv')

if 'run_num' not in st.session_state:

    st.session_state.run_num = st.session_state.rn_sb_df['Stable Beam Run Number'].values.tolist()[0]

if 'run_num_index' not in st.session_state:

    st.session_state.run_num_index = st.session_state.rn_sb_df['Stable Beam Run Number'][st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num].index.tolist()[0]

if 'chamber' not in st.session_state:

    st.session_state.chamber = st.session_state.chamber_list.iloc[0]['Chamber']

if 'partition' not in st.session_state:

    st.session_state.partition = st.session_state.chamber_list.iloc[0]['Partition']

if 'disabled' not in st.session_state:

    st.session_state.disabled = False

if 'comparison_1_run_num' not in st.session_state:

    st.session_state.comparison_1_run_num = 0
    
############################################# Sidebar #############################################

col1, col2 = st.sidebar.columns(2)

butt_next = col1.button('Previous Run', type = 'primary')

butt_prev = col2.button('Next Run', type = 'primary')

if butt_next == True:

    if st.session_state.run_num_index + 2 < st.session_state.rn_sb_df.shape[0]:

        st.session_state.run_num_index += 1
        
    else:

        st.session_state.run_num_index = 0
        
    butt_next = False
    
if butt_prev == True:

    st.session_state.run_num_index -= 1
    butt_prev = False

st.session_state.run_num = st.sidebar.selectbox(
    "Select a Run Number:",
    st.session_state.rn_sb_df['Stable Beam Run Number'],
    index = st.session_state.run_num_index
)

st.session_state.run_num_index = st.session_state.rn_sb_df['Stable Beam Run Number'][st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num].index.tolist()[0]

st.session_state.chamber = st.sidebar.selectbox(
"Select a Chamber:",
st.session_state.chamber_list['Chamber'],
index = 0
)

st.session_state.partition = st.session_state.chamber_list[st.session_state.chamber_list['Chamber'] == st.session_state.chamber]['Partition'].values.tolist()[0]

st.sidebar.checkbox("Use custom date range", value = st.session_state.disabled, key="disabled")

st.session_state.ts_start_input = st.sidebar.text_input(
    "Select a start time (eg. 2024-06-15 04:22:00)",
    '2024-07-01 00:00:00',
    disabled = not st.session_state.disabled
)

st.session_state.ts_end_input = st.sidebar.text_input(
    "Select an end time (eg. 2024-06-30 04:22:00)",
    '2024-07-03 00:00:00',
    disabled = not st.session_state.disabled
)

################################### Initialize Save Directories ###################################

if st.session_state.disabled == False:

    path = save_path + str(st.session_state.run_num)

    isExist = os.path.exists(path)
    
    if(not isExist):
        
        os.mkdir(path)
        os.mkdir(path + "/images")
        os.mkdir(path + "/csv")
        os.mkdir(path + "/chambers")
        os.chmod(path, stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/images", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/csv", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/chambers", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)

    chamber_path = path + "/chambers/" + st.session_state.chamber

    isExist = os.path.exists(chamber_path)
    
    if(not isExist):
        
        os.mkdir(chamber_path)
        os.chmod(chamber_path, stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)

    chamber_path = chamber_path + "/"

else:

    path = save_path + f"{since_dt}_{till_dt}"

    isExist = os.path.exists(path)
    
    if(not isExist):
        
        os.mkdir(path)
        os.mkdir(path + "/images")
        os.mkdir(path + "/csv")
        os.mkdir(path + "/chambers")
        os.chmod(path, stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/images", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/csv", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
        os.chmod(path + "/chambers", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)

    chamber_path = path + "/chambers/" + st.session_state.chamber

    isExist = os.path.exists(chamber_path)
    
    if(not isExist):
        
        os.mkdir(chamber_path)
        os.chmod(chamber_path, stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)

    chamber_path = chamber_path + "/"

############################# Extract Start and End Timestamp Queries #############################

if st.session_state.disabled == True:
    
    ts_start_dt = st.session_state.ts_start_input
    ts_end_dt = st.session_state.ts_end_input

    fmt = '%Y-%m-%d %H:%M:%S'                                       # For user input in the sidebar

    since_dt = dt.strptime(ts_start_dt, fmt)
    till_dt = dt.strptime(ts_end_dt, fmt)

    since_ts = int((pd.to_datetime(ts_start_dt)).timestamp()*1e6)       # Convert these to timestamps, convert to us, convert to int
    till_ts = int((pd.to_datetime(ts_end_dt)).timestamp()*1e6)
    
else:
    ts_start_dt = st.session_state.rn_sb_df[st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num]['Run Start dt'].values[0]
    ts_end_dt = st.session_state.rn_sb_df[st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num]['Run End dt'].values[0]

    since_ts = int((pd.to_datetime(ts_start_dt)).timestamp()*1e6)
    till_ts = int((pd.to_datetime(ts_end_dt)).timestamp()*1e6)

# Convert datetime to timestamp

since_dt = pd.Timestamp.fromtimestamp((since_ts-7200000000)/1e6).tz_localize('UTC').tz_convert('Europe/Zurich')
till_dt = pd.Timestamp.fromtimestamp((till_ts-7200000000)/1e6).tz_localize('UTC').tz_convert('Europe/Zurich')

############################################# Header ##############################################

col1, col2 = st.columns(2)

with col1:

    if st.session_state.disabled == False:
                
        st.write(f"## Run {st.session_state.run_num}")
    
    else:
    
        st.write(f"## {since_dt} - {till_dt}")

with col2:
    
    if st.session_state.run_num_index != 0:

        st.session_state.comparison_1_run_num = st.selectbox(
            "Select a Comparison Run Number:",
            st.session_state.rn_sb_df['Stable Beam Run Number'],
            index = st.session_state.run_num_index - 1,
            key = 'camparison_1'
        )

    else: 
        
        st.session_state.comparison_1_run_num = st.selectbox(
            "Select a Comparison Run Number:",
            st.session_state.rn_sb_df['Stable Beam Run Number'],
            index = st.session_state.run_num_index + 1,
            key = 'camparison_1'
        )

########################################### Histograms ############################################

col1, col2 = st.columns(2)

with col1:

    link = link_creator(st.session_state.run_num, 
                        st.session_state.partition, 
                        st.session_state.chamber, 
                        'HitsPerChannel', type = 'EXPERT')
    
    st.image(link)
    st.link_button('Link to Image', link)

###################################################

    link = link_creator(st.session_state.run_num, 
                        st.session_state.partition, 
                        st.session_state.chamber, 
                        'HitsPerTube_ge_ADC')
    
    st.image(link)
    st.link_button('Link to Image', link)

###################################################

    link = link_creator(st.session_state.run_num, 
                        st.session_state.partition, 
                        st.session_state.chamber, 
                        'HitRate_VS_LB')
    
    st.image(link)
    st.link_button('Link to Image', link)

###################################################
    
    link = link_creator(st.session_state.run_num, 
                        st.session_state.partition, 
                        st.session_state.chamber, 
                        'ErrorRatePerLB')
    
    st.image(link)
    st.link_button('Link to Image', link)

###################################################
    
    link = link_creator(st.session_state.run_num, 
                        st.session_state.partition, 
                        st.session_state.chamber, 
                        'ADC_ML1')
    
    st.image(link)
    st.link_button('Link to Image', link)

###################################################
    
    link = link_creator(st.session_state.run_num, 
                        st.session_state.partition, 
                        st.session_state.chamber, 
                        'ADC_ML2')
    
    st.image(link)
    st.link_button('Link to Image', link)

with col2:

    if st.session_state.comparison_1_run_num != 0:

        link = link_creator(st.session_state.comparison_1_run_num, 
                            st.session_state.partition, 
                            st.session_state.chamber, 
                            'HitsPerChannel', type = 'EXPERT')
        
        st.image(link)
        st.link_button('Link to Image', link)
    
    ###################################################
    
        link = link_creator(st.session_state.comparison_1_run_num, 
                            st.session_state.partition, 
                            st.session_state.chamber, 
                            'HitsPerTube_ge_ADC')
        
        st.image(link)
        st.link_button('Link to Image', link)
    
    ###################################################
    
        link = link_creator(st.session_state.comparison_1_run_num, 
                            st.session_state.partition, 
                            st.session_state.chamber, 
                            'HitRate_VS_LB')
        
        st.image(link)
        st.link_button('Link to Image', link)
    
    ###################################################
        
        link = link_creator(st.session_state.comparison_1_run_num, 
                            st.session_state.partition, 
                            st.session_state.chamber, 
                            'ErrorRatePerLB')
        
        st.image(link)
        st.link_button('Link to Image', link)
    
    ###################################################
        
        link = link_creator(st.session_state.comparison_1_run_num, 
                            st.session_state.partition, 
                            st.session_state.chamber, 
                            'ADC_ML1')
        
        st.image(link)
        st.link_button('Link to Image', link)
    
    ###################################################
        
        link = link_creator(st.session_state.comparison_1_run_num, 
                            st.session_state.partition, 
                            st.session_state.chamber, 
                            'ADC_ML2')
        
        st.image(link)
        st.link_button('Link to Image', link)

    else:

        st.stop()