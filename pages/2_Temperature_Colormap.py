import streamlit as st
# from streamlit_extras.switch_page_button import switch_page
import streamlit.components.v1 as stc
import pandas as pd
import numpy as np
from PIL import Image
import re
import gc
from datetime import datetime as dt
from datetime import timedelta
import sys, os, stat, base64

from functions import *

import plotly
import plotly.graph_objects as go
from beauty import Beauty
import libpbeastpy

db = libpbeastpy.ServerProxy('http://pc-tbed-bst-prod:8080')    # http://pc-tbed-bst-03:8080 is down
beauty = Beauty('http://pc-tbed-bst-prod:8080')

# setup streamlit

st.set_page_config(layout="wide", 
                   page_title="MDT DCS Data Viewer",
                   page_icon="📊")

if 'fsm_type' not in st.session_state:
    
    st.session_state.fsm_type = 'JTAG'
    
st.write(f"""
# {st.session_state.fsm_type} Chamber Temperature Colormap
""")

if 'csv_path' not in st.session_state:
    
    st.session_state.csv_path = 'mdt_dcs_offline/streamlit/mdt-dcs-saved-run-analysis'          # Not currently used

try:                                                                                            # Overarching error handling for pbeast errors
    
    if 'rn_sb_df' not in st.session_state:
    
        st.session_state.rn_sb_df = generate_rn_sb_df(dt(2024,4,1,0,0), dt.now())               # Generates all run numbers available on the pbeast server that achieve stable beam
    
    if 'chamber_list' not in st.session_state:
        
        st.session_state.chamber_list = pd.read_csv('mappings/chamb_partition_list.csv')                 # Loads the data frame of chamber names and their respective partitions
    
    if 'run_num' not in st.session_state:
    
        st.session_state.run_num = st.session_state.rn_sb_df['Stable Beam Run Number'].values.tolist()[0]       # Loads the most recent run from the generated run list. Maybe there is a better way to extract the integer value from a pd df
    
    if 'run_num_index' not in st.session_state:
    
        st.session_state.run_num_index = st.session_state.rn_sb_df['Stable Beam Run Number'][st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num].index.tolist()[0]     # extracts the index number of the current run from the run df

    if 'disabled' not in st.session_state:

        st.session_state.disabled = False

    if 'chamber' not in st.session_state:
    
        st.session_state.chamber = st.session_state.chamber_list.iloc[0]['Chamber']
        
    ## Sidebar
    
    col1, col2 = st.sidebar.columns(2)
    
    butt_next = col1.button('Previous Run', type = 'primary')
    
    butt_prev = col2.button('Next Run', type = 'primary')
    
    if butt_next == True:

        if st.session_state.run_num_index + 2 < st.session_state.rn_sb_df.shape[0]:
    
            st.session_state.run_num_index += 1
            
        else:

            st.session_state.run_num_index = 0
            
        butt_next = False
    
    if butt_prev == True:
    
        st.session_state.run_num_index -= 1
        butt_prev = False
    
    st.session_state.run_num = st.sidebar.selectbox(
        "Select a Run Number:",
        st.session_state.rn_sb_df['Stable Beam Run Number'],
        index = st.session_state.run_num_index
    )

    # run_path = save_path + str(st.session_state.run_num)          # Future implementation of initializing the shaved run files
    
    # isExist = os.path.exists(run_path)
    
    # if not isExist:
    #     #create run folder
    #     os.mkdir(run_path)
    #     os.chmod(run_path, stat.S_IRWXG | stat.S_IRWXU |stat.S_IRWXO)
    
    st.session_state.run_num_index = st.session_state.rn_sb_df['Stable Beam Run Number'][st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num].index.tolist()[0]         # If the run number is changed in the sidebar, reevaluate the index
    
    st.sidebar.checkbox("Use custom date range", value = st.session_state.disabled, key="disabled")
    
    st.session_state.ts_start_input = st.sidebar.text_input(
        "Select a start time (eg. 2024-06-15 04:22:00)",
        '2024-07-01 00:00:00',
        disabled = not st.session_state.disabled
    )
    
    st.session_state.ts_end_input = st.sidebar.text_input(
        "Select an end time (eg. 2024-06-30 04:22:00)",
        '2024-07-03 00:00:00',
        disabled = not st.session_state.disabled
    )
    
    fmt = '%Y-%m-%d %H:%M:%S'                                       # For user input in the sidebar

    if st.session_state.disabled == True:                            # If the custom date range option is selected
        ts_start_dt = st.session_state.ts_start_input
        ts_end_dt = st.session_state.ts_end_input
    
        since_dt = dt.strptime(ts_start_dt, fmt)
        till_dt = dt.strptime(ts_end_dt, fmt)

        since_ts = int((pd.to_datetime(ts_start_dt)).timestamp()*1e6)       # Convert these to timestamps, convert to us, convert to int
        till_ts = int((pd.to_datetime(ts_end_dt)).timestamp()*1e6)
        
    else:                                                            # If the run number selection is active
        ts_start_dt = st.session_state.rn_sb_df[st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num]['Run Start dt'].values[0]      # Strip start and end times of the run from the run df
        ts_end_dt = st.session_state.rn_sb_df[st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num]['Run End dt'].values[0]
    
        since_ts = int((pd.to_datetime(ts_start_dt)).timestamp()*1e6)       # Convert these to timestamps, convert to us, convert to int
        till_ts = int((pd.to_datetime(ts_end_dt)).timestamp()*1e6)

    two_hrs_in_us = int(2*60*60*1e6)                                 # 2 hrs * 60 min * 60 s * 1e6 us

    since_dt = pd.Timestamp.fromtimestamp((since_ts-two_hrs_in_us)/1e6).tz_localize('UTC').tz_convert('Europe/Zurich')
    till_dt = pd.Timestamp.fromtimestamp((till_ts-two_hrs_in_us)/1e6).tz_localize('UTC').tz_convert('Europe/Zurich')
    
    # Create Run Info df
    
    if st.session_state.disabled == False:
    
        run_info_df = st.session_state.rn_sb_df[st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num]

        sb_since_dt = since_dt + timedelta(hours = 2)

        sb_till_dt = till_dt + timedelta(hours=2)
        
        sbFlag = beauty.timeseries(sb_since_dt.tz_convert(None), 
                                   sb_till_dt.tz_convert(None), 
                                   'DCS', 
                                   'ATLAS_PVSSDCS', 
                                   'value-number', 
                                   'ATLGCSLHC:bit_lhc_RunCtrl_SafeBeam_StableBeams.value', 
                                   None, 
                                   False)[0]

        sb_df = pd.DataFrame({'value': sbFlag.array , 'dt': sbFlag.index})
        sb_df['value'] = sb_df['value'].astype(float).astype(int)
        sb_df['ts'] = sb_df['dt'].values.astype(int) // 10**3
        
        sb_start_dt = sb_df.iloc[0]['dt'].tz_convert('Europe/Zurich')
        sb_end_dt = sb_df.iloc[1]['dt'].tz_convert('Europe/Zurich')
        
        sb_start_ts = sb_df.iloc[0]['ts']
        sb_end_ts = sb_df.iloc[1]['ts']
    
    
        run_info_df['Run Start dt'] = str(run_info_df['Run Start dt'].iloc[0])
        run_info_df['Run End dt'] = str(run_info_df['Run End dt'].iloc[0])
        
        total_run_time = till_dt - since_dt
        run_info_df['Total Run Time'] = str(total_run_time)[:-3]
        
        lbn_sb_r4p_df = generate_lbn_sb_r4p_df(since_dt.tz_convert(None), till_dt.tz_convert(None))
        run_info_df['Total Lumi Block'] = int(lbn_sb_r4p_df['LumiBlock Number'].max())
        
        run_info_df['Stable Beam Start dt'] = sb_start_dt
        run_info_df['Stable Beam End dt'] = sb_end_dt
        
        sb_lb_start = int(lbn_sb_r4p_df[lbn_sb_r4p_df['StableBeam'] == 1.0]['LumiBlock Number'].min(numeric_only = True))
        sb_lb_end = int(lbn_sb_r4p_df[lbn_sb_r4p_df['StableBeam'] == 1.0]['LumiBlock Number'].max(numeric_only = True))
        
        total_sb_lb = sb_lb_end - sb_lb_start
        
        run_info_df['Stable Beam LumiBlock Range'] = str(sb_lb_start) + ' - ' + str(sb_lb_end)
        
        run_info_df = run_info_df.T.rename(columns={run_info_df.T.columns[0]: ""})
        
        ## Header
        
        st.write(f"## Run {st.session_state.run_num}")

    else:

        st.write(f"## {since_dt} - {till_dt}")
    
    #set path for output files and check if exists


    if st.session_state.disabled == False:

        path = '/eos/user/m/mdtoffline/mdt_dcs_offline/streamlit/mdt-dcs-saved-run-analysis/' + str(st.session_state.run_num)

    else:

        path = '/eos/user/m/mdtoffline/mdt_dcs_offline/streamlit/mdt-dcs-saved-run-analysis/' + f"{since_dt}_{till_dt}"
    
    isExist = os.path.exists(path)
    if(not isExist):
        #create paths
        os.mkdir(path)
        os.mkdir(path + "/images")
        os.mkdir(path + "/root")
        os.mkdir(path + "/csv")

#####################################################

    with st.spinner('Offloading Temperature Data...'):
    
        temp_sensors_df = db.get_data('DCS', 
                                      'ATLAS_PVSSMDT', 
                                      'value-number', 
                                      '.*.ntc.*', 
                                      True, 
                                      since_ts, 
                                      till_ts, 
                                      0, 
                                      True)
        
        temp_sensors_df = queryDataToDataFrame(temp_sensors_df)
        temp_sensors_df.sort_values(by=['ts'], inplace=True)
        temp_sensors_df['dt'] = pd.to_datetime(temp_sensors_df['ts'], unit = 'us', utc = True).dt.tz_convert('Europe/Zurich')
        temp_sensors_df['value'] = temp_sensors_df['value'].astype('float64')
        temp_sensors_df['Sensor'] = temp_sensors_df['datapoint'].apply(lambda x : x.split('.')[-2]).apply(lambda x : list(x)[-2:]).apply(lambda x : int(''.join(x)))
        
        temp_sensors_df['chamber'] = temp_sensors_df['datapoint'].apply(lambda x : x.split(':')[1][:7])
        unique_chambers = temp_sensors_df['chamber'].unique()
        
        avg_temps = []
        chambers = []
        faulty_chambers = []
        faulty_sensors = []
        
        for chamber in unique_chambers:
        
            tmp_df = temp_sensors_df[temp_sensors_df['chamber'] == chamber]
        
            number_of_sensors = tmp_df['Sensor'].max()
        
            avg_sensors = []
            
            for sensor in range(1, number_of_sensors + 1):
        
                tmp_sensor_df = tmp_df[tmp_df['Sensor'] == sensor]
        
                avg_sensor = tmp_sensor_df['value'].mean()
        
                if avg_sensor > 100 or avg_sensor == 0.0:
        
                    avg_sensor = np.nan
        
                    faulty_chambers.append(chamber)
                    faulty_sensors.append(sensor)
                    
                avg_sensors.append(avg_sensor)
        
            avg_temp = np.nanmean(avg_sensors)
            
            avg_temps.append(avg_temp)
            chambers.append(chamber)
        
        tmp_dict = {'Chamber' : chambers, 'Average Temperature' : avg_temps}
        
        avg_temp_df = pd.DataFrame(tmp_dict)
        
        tmp_dict = {'Chamber' : faulty_chambers, 'Faulty Sensors' : faulty_sensors}
        
        faulty_temp_sensors_df = pd.DataFrame(tmp_dict)
        
        max_temp = np.nanmax(avg_temp_df['Average Temperature'])
        min_temp = np.nanmin(avg_temp_df['Average Temperature'])

################################################### PLOTS ######################################################
    
    col1, col2, col3 = st.columns(3)
    
    with col1:

        eic_coords_df = pd.read_parquet('mappings/eic_coords.parquet')

        event = st.plotly_chart(plot_ei(avg_temp_df, 'C', temp = True, max_temp = max_temp, min_temp = min_temp), use_container_width=True, on_select = 'rerun')

        points = event['selection']['points']

        if points != []:
        
            points = event['selection']['points'][0]
        
            x = round(float(points['x']), 2)
            y = round(float(points['y']), 2)
        
            st.session_state.chamber = eic_coords_df[(eic_coords_df['x0'] == x) & (eic_coords_df['y0'] == y)].iloc[0]['chamber']
        
            st.switch_page('pages/2_Chamber_Selection.py')

###################################################

        emc_coords_df = pd.read_parquet('mappings/emc_coords.parquet')
            
        event = st.plotly_chart(plot_em(avg_temp_df, 'C', temp = True, max_temp = max_temp, min_temp = min_temp), use_container_width=True, on_select = 'rerun')

        points = event['selection']['points']

        if points != []:
        
            points = event['selection']['points'][0]
        
            x = round(float(points['x']), 2)
            y = round(float(points['y']), 2)
        
            st.session_state.chamber = emc_coords_df[(emc_coords_df['x0'] == x) & (emc_coords_df['y0'] == y)].iloc[0]['chamber']
        
            st.switch_page('pages/2_Chamber_Selection.py')
            
###################################################
        
        eoc_coords_df = pd.read_parquet('mappings/eoc_coords.parquet')

        event = st.plotly_chart(plot_eo(avg_temp_df, 'C', temp = True, max_temp = max_temp, min_temp = min_temp), use_container_width=True, on_select = 'rerun')

        points = event['selection']['points']

        if points != []:
        
            points = event['selection']['points'][0]
        
            x = round(float(points['x']), 2)
            y = round(float(points['y']), 2)
        
            st.session_state.chamber = eoc_coords_df[(eoc_coords_df['x0'] == x) & (eoc_coords_df['y0'] == y)].iloc[0]['chamber']
        
            st.switch_page('pages/2_Chamber_Selection.py')

###################################################
    
    with col2:

        be_bi_coords_df = pd.read_parquet('mappings/be_bi_coords.parquet')
        
        event = st.plotly_chart(plot_be_bi(be_bi_coords_df, avg_temp_df, temp = True, max_temp = max_temp, min_temp = min_temp), use_container_width=True, on_select = 'rerun')

        points = event['selection']['points']

        if points != []:
        
            points = event['selection']['points'][0]
        
            x = int(points['x']-5)
            y = int(points['y']-5)
        
            st.session_state.chamber = be_bi_coords_df[(be_bi_coords_df['x0'] == x) & (be_bi_coords_df['y0'] == y)].iloc[0]['chamber']
        
            st.switch_page('pages/2_Chamber_Selection.py')

###################################################
        
        bm_coords_df = pd.read_parquet('mappings/bm_coords.parquet')
        
        event = st.plotly_chart(plot_bm(bm_coords_df, avg_temp_df, temp = True, max_temp = max_temp, min_temp = min_temp), use_container_width=True, on_select = 'rerun')
        
        points = event['selection']['points']

        if points != []:
        
            points = event['selection']['points'][0]
        
            x = int(points['x']-5)
            y = int(points['y']-5)
        
            st.session_state.chamber = bm_coords_df[(bm_coords_df['x0'] == x) & (bm_coords_df['y0'] == y)].iloc[0]['chamber']
        
            st.switch_page('pages/2_Chamber_Selection.py')

###################################################
        
        bo_coords_df = pd.read_parquet('mappings/bo_coords.parquet')
        
        event = st.plotly_chart(plot_bo(bo_coords_df, avg_temp_df, temp = True, max_temp = max_temp, min_temp = min_temp), use_container_width=True, on_select = 'rerun')
        
        points = event['selection']['points']

        if points != []:
        
            points = event['selection']['points'][0]
        
            x = int(points['x']-5)
            y = int(points['y']-5)
        
            st.session_state.chamber = bo_coords_df[(bo_coords_df['x0'] == x) & (bo_coords_df['y0'] == y)].iloc[0]['chamber']
        
            st.switch_page('pages/2_Chamber_Selection.py')

###################################################
    
    with col3:
        
        eia_coords_df = pd.read_parquet('mappings/eia_coords.parquet')
        
        event = st.plotly_chart(plot_ei(avg_temp_df, 'A', temp = True, max_temp = max_temp, min_temp = min_temp), use_container_width=True, on_select = 'rerun')

        points = event['selection']['points']

        if points != []:
        
            points = event['selection']['points'][0]
        
            x = round(float(points['x']), 2)
            y = round(float(points['y']), 2)
        
            st.session_state.chamber = eia_coords_df[(eia_coords_df['x0'] == x) & (eia_coords_df['y0'] == y)].iloc[0]['chamber']
        
            st.switch_page('pages/2_Chamber_Selection.py')
            
###################################################

        ema_coords_df = pd.read_parquet('mappings/ema_coords.parquet')
        
        event = st.plotly_chart(plot_em(avg_temp_df, 'A', temp = True, max_temp = max_temp, min_temp = min_temp), use_container_width=True, on_select = 'rerun')

        points = event['selection']['points']

        if points != []:
        
            points = event['selection']['points'][0]
        
            x = round(float(points['x']), 2)
            y = round(float(points['y']), 2)
        
            st.session_state.chamber = ema_coords_df[(ema_coords_df['x0'] == x) & (ema_coords_df['y0'] == y)].iloc[0]['chamber']
        
            st.switch_page('pages/2_Chamber_Selection.py')
            
###################################################

        eoa_coords_df = pd.read_parquet('mappings/eoa_coords.parquet')

        event = st.plotly_chart(plot_eo(avg_temp_df, 'A', temp = True, max_temp = max_temp, min_temp = min_temp), use_container_width=True, on_select = 'rerun')

        points = event['selection']['points']

        if points != []:
        
            points = event['selection']['points'][0]
        
            x = round(float(points['x']), 2)
            y = round(float(points['y']), 2)
        
            st.session_state.chamber = eoa_coords_df[(eoa_coords_df['x0'] == x) & (eoa_coords_df['y0'] == y)].iloc[0]['chamber']
        
            st.switch_page('pages/2_Chamber_Selection.py')
            
#######################################################################################################################
    
    if avg_temp_df.shape[0] > 0:
    
        st.dataframe(avg_temp_df,
                     hide_index=True, 
                     use_container_width = True)
        
        st.write(f'{avg_temp_df.shape[0]} Chamber temperatures for this time range.')

        if faulty_temp_sensors_df.shape[0] > 0:

            st.dataframe(faulty_temp_sensors_df,
                         hide_index=True, 
                         use_container_width = True)
            
            st.write(f'{faulty_temp_sensors_df.shape[0]} Faulty temperature sensors for this time range.')

        else:

            st.write('No faulty temperature sensors detected for this time range.')
    
    else:
    
        st.write('No temperature data for this time range.')

except RuntimeError:

    st.write('## P-BEAST server currently unavailable!')