# app/Dockerfile

FROM gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:el9

WORKDIR /app

RUN yum install -y python3
RUN yum install -y python3-pip

COPY . .

RUN python3 -m venv myvenv
RUN source myvenv/bin/activate
RUN myvenv/bin/python3 -m pip install --upgrade pip
RUN myvenv/bin/pip install -r requirements.txt
# RUN NONINTERACTIVE=1 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
# RUN brew install node@22

EXPOSE 8501

ENTRYPOINT ["/bin/bash", "/app/tdaqSetup.sh"]

