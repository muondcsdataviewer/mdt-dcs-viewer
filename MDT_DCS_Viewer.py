######################################### Import Libaries #########################################

import streamlit as st
import pandas as pd
import numpy as np
import re
import gc
from datetime import datetime as dt
import datetime
from datetime import timedelta
import os, stat
import requests

from functions import *

import plotly.graph_objects as go
from plotly.subplots import make_subplots
from beauty import Beauty
import libpbeastpy

########################################### Initialize ############################################

# setup pbeast python API

db = libpbeastpy.ServerProxy('http://pc-tbed-bst-prod:8080')    # http://pc-tbed-bst-03:8080 is down
beauty = Beauty('http://pc-tbed-bst-prod:8080')

# setup streamlit

st.set_page_config(layout="wide", 
                   page_title="MDT DCS Data Viewer",
                   page_icon="📊")

st.write("""
# MDT DCS Data Viewer
""")

# st.cache_data.clear()                                         # Testing cleanup during initial openshift hosting (memory overload has since been fixed)

## Load Session State Variables from MDT_DCS_Viewer Home Page (streamlit-saved variables for each user session between pages when navigating to the webapp)

save_path = '/eos/user/m/mdtoffline/mdt_dcs_offline/streamlit/mdt-dcs-saved-run-analysis/'      # For future saving functionality

empty_df = pd.DataFrame({'datapoint': np.array([], dtype=str), 'ts' : np.array([], dtype=int), 'value' : np.array([], dtype=int)})

if 'csv_path' not in st.session_state:
    
    st.session_state.csv_path = 'mdt_dcs_offline/streamlit/mdt-dcs-saved-run-analysis'          # Not currently used

if 'pbeast_mapping_df' not in st.session_state:

    st.session_state.pbeast_mapping_df =  pd.read_csv('mappings/Pbeast_mapping.csv').drop(columns = 'Unnamed: 0')

if 'rn_sb_df' not in st.session_state:
    
    try:
        
        st.session_state.rn_sb_df = generate_rn_sb_df(dt(2024,4,1,0,0), dt.now())               # Generates all run numbers available on the pbeast server that achieve stable beam
        st.session_state.rn_sb_df.to_parquet(save_path + 'stable_beam_runs_df' + '.parquet')

    except:

        st.session_state.rn_sb_df = pd.read_parquet(save_path + 'stable_beam_runs_df' + '.parquet')

if 'chamber_list' not in st.session_state:
    
    st.session_state.chamber_list = pd.read_csv('mappings/chamb_partition_list.csv')                 # Loads the data frame of chamber names and their respective partitions

if 'run_num' not in st.session_state:

    st.session_state.run_num = st.session_state.rn_sb_df.iloc[0]['Stable Beam Run Number']       # Loads the most recent run from the generated run list. Maybe there is a better way to extract the integer value from a pd df

if 'run_num_index' not in st.session_state:

    st.session_state.run_num_index = st.session_state.rn_sb_df[st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num].index.tolist()[0]     # extracts the index number of the current run from the run df

if 'disabled' not in st.session_state:

    st.session_state.disabled = False
    
if 'start_strftime' not in st.session_state:

    st.session_state.start_strftime = '2024-07-01 00:00:00'

if 'end_strftime' not in st.session_state:

    st.session_state.end_strftime = '2024-07-03 00:00:00'
    
## Sidebar

col1, col2 = st.sidebar.columns(2)

butt_next = col1.button('Previous Run', type = 'primary')

butt_prev = col2.button('Next Run', type = 'primary')

if butt_next == True:

    if st.session_state.run_num_index + 2 < st.session_state.rn_sb_df.shape[0]:

        st.session_state.run_num_index += 1
        
    else:

        st.session_state.run_num_index = 0
        
    butt_next = False

if butt_prev == True:

    st.session_state.run_num_index -= 1
    butt_prev = False

st.session_state.run_num = st.sidebar.selectbox(
    "Select a Run Number:",
    st.session_state.rn_sb_df['Stable Beam Run Number'],
    index = st.session_state.run_num_index
)

st.session_state.run_num_index = st.session_state.rn_sb_df['Stable Beam Run Number'][st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num].index.tolist()[0]         # If the run number is changed in the sidebar, reevaluate the index

st.sidebar.checkbox("Use custom date range", value = st.session_state.disabled, key="disabled")

fmt = '%Y-%m-%d %H:%M:%S'                                       # For user input in the sidebar


if st.session_state.disabled == True:                            # If the custom date range option is selected
    
    ts_start_dt = st.session_state.ts_start_input
    ts_end_dt = st.session_state.ts_end_input
    
    fmt = '%Y-%m-%d %H:%M:%S'                                       # For user input in the sidebar

    since_dt = dt.strptime(ts_start_dt, fmt)
    till_dt = dt.strptime(ts_end_dt, fmt)

    since_ts = int((pd.to_datetime(ts_start_dt)).timestamp()*1e6)       # Convert these to timestamps, convert to us, convert to int
    till_ts = int((pd.to_datetime(ts_end_dt)).timestamp()*1e6)

    if since_dt > till_dt:
    
        st.write('Please select an end date that is later than the start date')
    
        st.stop()
    
else:                                                            # If the run number selection is active
    
    ts_start_dt = st.session_state.rn_sb_df[st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num]['Run Start dt'].iloc[0]      # Strip start and end times of the run from the run df
    ts_end_dt = st.session_state.rn_sb_df[st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num]['Run End dt'].iloc[0]

    since_ts = int((pd.to_datetime(ts_start_dt)).timestamp()*1e6)       # Convert these to timestamps, convert to us, convert to int
    till_ts = int((pd.to_datetime(ts_end_dt)).timestamp()*1e6)

    st.session_state.start_strftime = ts_start_dt.strftime(fmt)
    st.session_state.end_strftime = ts_end_dt.strftime(fmt)

st.session_state.ts_start_input = st.sidebar.text_input(
    "Select a start time (eg. 2024-06-15 04:22:00)",
    '2024-07-01 00:00:00',
    disabled = not st.session_state.disabled
)

st.session_state.ts_end_input = st.sidebar.text_input(
    "Select an end time (eg. 2024-06-30 04:22:00)",
    '2024-07-03 00:00:00',
    disabled = not st.session_state.disabled
)

## Set start query, end query, and save paths ##

two_hrs_in_us = int(2*60*60*1e6)                                 # 2 hrs * 60 min * 60 s * 1e6 us

since_dt = pd.Timestamp.fromtimestamp((since_ts-two_hrs_in_us)/1e6).tz_localize('UTC').tz_convert('Europe/Zurich')
till_dt = pd.Timestamp.fromtimestamp((till_ts-two_hrs_in_us)/1e6).tz_localize('UTC').tz_convert('Europe/Zurich')

if st.session_state.disabled == False:

    path = '/eos/user/m/mdtoffline/mdt_dcs_offline/streamlit/mdt-dcs-saved-run-analysis/' + str(st.session_state.run_num)

else:

    path = '/eos/user/m/mdtoffline/mdt_dcs_offline/streamlit/mdt-dcs-saved-run-analysis/' + f"{since_dt}_{till_dt}"

isExist = os.path.exists(path)

if not isExist:
    
    #create paths
    os.mkdir(path)
    os.mkdir(path + "/images")
    os.mkdir(path + "/csv")
    os.mkdir(path + "/chambers")
    os.chmod(path, stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
    os.chmod(path + "/images", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
    os.chmod(path + "/csv", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)
    os.chmod(path + "/chambers", stat.S_IRWXG | stat.S_IRWXU | stat.S_IRWXO)

# Create Run Info df

if st.session_state.disabled == False:

    run_info_df = st.session_state.rn_sb_df[st.session_state.rn_sb_df['Stable Beam Run Number'] == st.session_state.run_num]

    tmp_path = path + '/csv/' + 'sb_df' + '.parquet'

    isExist = os.path.exists(tmp_path)

    if not isExist:

        sb_since_dt = since_dt + timedelta(hours = 2) - timedelta(minutes = 20)

        sb_till_dt = till_dt + timedelta(hours = 2) + timedelta(minutes = 20)

        sbFlag = beauty.timeseries(sb_since_dt.tz_convert(None), 
                                   sb_till_dt.tz_convert(None), 
                                   'DCS', 
                                   'ATLAS_PVSSDCS', 
                                   'value-number', 
                                   'ATLGCSLHC:bit_lhc_RunCtrl_SafeBeam_StableBeams.value', 
                                   None, 
                                   False)[0]

        sb_df = pd.DataFrame({'value': sbFlag.array , 'dt': sbFlag.index})
        sb_df['value'] = sb_df['value'].astype(float).astype(int)
        sb_df['ts'] = sb_df['dt'].values.astype(int) // 10**3

        sb_df.to_parquet(tmp_path)

    else:

        sb_df = pd.read_parquet(tmp_path)

    tmp_path = path + '/csv/' + 'lbn_sb_r4p_df' + '.csv'

    isExist = os.path.exists(tmp_path)

    if not isExist:        

        lbn_sb_r4p_df = generate_lbn_sb_r4p_df(since_dt.tz_convert(None), till_dt.tz_convert(None))
        lbn_sb_r4p_df.to_parquet(tmp_path)

    else:

        try:

            lbn_sb_r4p_df = pd.read_parquet(tmp_path)

        except:

            pass

    tmp_path = path + '/csv/' + 'run_info_df' + '.csv'

    isExist = os.path.exists(tmp_path)

    if not isExist:
    
        sb_start_dt = sb_df.iloc[0]['dt'].tz_convert('Europe/Zurich')
        sb_end_dt = sb_df.iloc[1]['dt'].tz_convert('Europe/Zurich')
        
        sb_start_ts = sb_df.iloc[0]['ts']
        sb_end_ts = sb_df.iloc[1]['ts']
    
        run_info_df['Run Start dt'] = str(run_info_df['Run Start dt'].iloc[0])
        run_info_df['Run End dt'] = str(run_info_df['Run End dt'].iloc[0])
        
        total_run_time = till_dt - since_dt
        run_info_df['Total Run Time'] = str(total_run_time)[:-3]
        
        run_info_df['Total Lumi Block'] = int(lbn_sb_r4p_df['LumiBlock Number'].max())
        
        run_info_df['Stable Beam Start dt'] = sb_start_dt
        run_info_df['Stable Beam End dt'] = sb_end_dt
        
        sb_lb_start = int(lbn_sb_r4p_df[lbn_sb_r4p_df['StableBeam'] == 1.0]['LumiBlock Number'].min(numeric_only = True))
        sb_lb_end = int(lbn_sb_r4p_df[lbn_sb_r4p_df['StableBeam'] == 1.0]['LumiBlock Number'].max(numeric_only = True))
        
        total_sb_lb = sb_lb_end - sb_lb_start
        
        run_info_df['Stable Beam LumiBlock Range'] = str(sb_lb_start) + ' - ' + str(sb_lb_end)

        run_info_df.to_csv(tmp_path)
        
        run_info_df = run_info_df.T.rename(columns={run_info_df.T.columns[0]: ""})

    else:

        run_info_df = pd.read_csv(tmp_path)

        run_info_df = run_info_df.T.rename(columns={run_info_df.T.columns[0]: ""})
        run_info_df.drop(index = ['Unnamed: 0'], inplace = True)
        
        sb_lb_start = int(lbn_sb_r4p_df[lbn_sb_r4p_df['StableBeam'] == 1.0]['LumiBlock Number'].min(numeric_only = True))
        sb_lb_end = int(lbn_sb_r4p_df[lbn_sb_r4p_df['StableBeam'] == 1.0]['LumiBlock Number'].max(numeric_only = True))
        
        total_sb_lb = sb_lb_end - sb_lb_start
    
    ## Header
    
    st.write(f"## Run {st.session_state.run_num}")
    
    col1, col2 = st.columns(2)
    
    with col1:
        
        st.dataframe(run_info_df, hide_index = False, use_container_width = True)

        link = f'https://atlas-datasummary.web.cern.ch/2024/run.py?run={st.session_state.run_num}'

        col11, col22 = st.columns(2)

        with col11:
    
            st.link_button('Link to Full Run Summary', link)

        with col22:

            if st.button('Requery Run Info'):

                os.remove(tmp_path) 
                st.rerun()
    
    with col2:

        if st.session_state.run_num <= 486894:

            link = f'https://atlas-datasummary.web.cern.ch/2024/rundata/run{st.session_state.run_num}/details/run{st.session_state.run_num}_lumi.png'
        else:

            link = f'https://atlas-datasummary.web.cern.ch/2024hi/rundata/run{st.session_state.run_num}/details/run{st.session_state.run_num}_lumi.png'
    
        st.image(link)

else:

    sb_since_dt = since_dt + timedelta(hours = 2) - timedelta(minutes = 20)

    sb_till_dt = till_dt + timedelta(hours = 2) + timedelta(minutes = 20)
    
    sbFlag = beauty.timeseries(sb_since_dt.tz_convert(None), 
                               sb_till_dt.tz_convert(None), 
                               'DCS', 
                               'ATLAS_PVSSDCS', 
                               'value-number', 
                               'ATLGCSLHC:bit_lhc_RunCtrl_SafeBeam_StableBeams.value', 
                               None, 
                               False)[0]

    sb_df = pd.DataFrame({'value': sbFlag.array , 'dt': sbFlag.index})
    sb_df['value'] = sb_df['value'].astype(float).astype(int)
    sb_df['ts'] = sb_df['dt'].values.astype(int) // 10**3

    sb_df.to_parquet(path + '/csv/' + 'sb_df' + '.parquet')

    st.write(f"## {since_dt - timedelta(hours = 2)} - {till_dt - timedelta(hours = 2)}")

## Flags

st.write("## Stable Beam FSM Data Quality")

tmp_path = path + '/csv/' + 'mdt_fsm_flags_df' + '.parquet'
fsm_csv_path = tmp_path

isExist = os.path.exists(tmp_path)

if not isExist:

    try:
    
        mdt_fsm_status_df = query_lib_data(since_ts, till_ts, '.*fsm.currentState', 'MDT', 'string')
    
        mdt_fsm_flags_df  = mdt_fsm_status_df[(mdt_fsm_status_df['value'] != "'OFF'") & 
                                              (mdt_fsm_status_df['value'] != "'ON'") & 
                                              (mdt_fsm_status_df['value'] != "'STANDBY'") & 
                                              (mdt_fsm_status_df['value'] != "'INITIALIZED'") & 
                                              (mdt_fsm_status_df['value'] != "'RAMP_UP'") & 
                                              (mdt_fsm_status_df['value'] != "'RAMPING'") & 
                                              (mdt_fsm_status_df['value'] != "'RAMP_DOWN'") & 
                                              (mdt_fsm_status_df['value'] != "'REQUEST'") & 
                                              (mdt_fsm_status_df['value'] != "'PRELOAD'") & 
                                              (mdt_fsm_status_df['value'] != "'VERIFY'") & 
                                              (mdt_fsm_status_df['value'] != "'RESET'") & 
                                              (mdt_fsm_status_df['value'] != "'STRINGLOAD'") & 
                                              (mdt_fsm_status_df['value'] != "'READY'")]

        del mdt_fsm_status_df
        gc.collect()
    
        mdt_fsm_flags_df['ts'] = mdt_fsm_flags_df['ts'].astype(int)

        mdt_fsm_flags_df.to_parquet(path + '/csv/' + 'mdt_fsm_flags_df' + '.parquet')

    except:

        mdt_fsm_flags_df = empty_df

        mdt_fsm_flags_df.to_parquet(path + '/csv/' + 'mdt_fsm_flags_df' + '.parquet')

else:

    mdt_fsm_flags_df = pd.read_parquet(tmp_path)

options = st.multiselect(
"FSM Type",
["JTAG", "HV", "LV"],
["JTAG", "HV", "LV"],
)

if len(options) == 0:

    st.write('Please Select an FSM Type')

    st.stop()

string_tmp = ''

for option in options:

    if string_tmp != '':

        string_tmp += '|'

    if option == 'HV':

        string_tmp += '.*_ML1.*fsm.currentState|.*_ML2.*fsm.currentState'

    else:

        string_tmp += '.*_' + option + '.*fsm.currentState'
    
mdt_fsm_flags_df = mdt_fsm_flags_df[mdt_fsm_flags_df['datapoint'].str.contains(string_tmp)]

if st.session_state.disabled == False:

    if mdt_fsm_flags_df.shape[0] > 0:
    
        lbn_sb_fsm_flags_df = generate_lbn_sb_fsm_flags_df(mdt_fsm_flags_df, lbn_sb_r4p_df)

        # lbn_sb_fsm_flags_df = lbn_sb_fsm_flags_df[lbn_sb_fsm_flags_df['Stable Beam'] == 1.0]
        
        fsm_pie_charts, lbn_sb_fsm_flags_df, fsm_percentage_for_sectors = generate_pie_charts(lbn_sb_fsm_flags_df, st.session_state.chamber_list, total_sb_lb)
        
        percentages, percentages_df = generate_fsm_percentages(lbn_sb_fsm_flags_df, sb_lb_start, sb_lb_end)

        lbn_sb_fsm_flags_df.to_parquet(path + '/csv/' + 'lbn_sb_fsm_flags_df' + '.parquet')
        percentages_df.to_parquet(path + '/csv/' + 'percentages' + '.parquet')

    
    else:
    
        fsm_pie_charts, fsm_percentage_for_sectors = generate_perfect_dq_pie_charts(st.session_state.chamber_list)

        lbn_sb_fsm_flags_df = empty_df

        sb_range = list(range(sb_lb_start, sb_lb_end))

        percentages = [(sb_range, [100.0] * len(sb_range), 'JTAG DQ Percentage'), 
                       (sb_range, [100.0] * len(sb_range), 'HV DQ Percentage'), 
                       (sb_range, [100.0] * len(sb_range), 'LV DQ Percentage')]

        percentages_df = pd.DataFrame({'Stable Beam LumiBlock Number' : sb_range, 
                                       'JTAG DQ Percentage' : [100.0] * len(sb_range), 
                                       'HV DQ Percentage' : [100.0] * len(sb_range), 
                                       'LV DQ Percentage' : [100.0] * len(sb_range)})        

        lbn_sb_fsm_flags_df.to_parquet(path + '/csv/' + 'lbn_sb_fsm_flags_df' + '.parquet')
        percentages_df.to_parquet(path + '/csv/' + 'percentages' + '.parquet')

    lbn_lumi_df = generate_lumi_lbn_df(since_ts, till_ts)

    lbn_lumi_df.to_parquet(path + '/csv/' + 'lbn_lumi_df' + '.parquet')

    t1 = lbn_lumi_df['LumiBlock Number']
    y1 = lbn_lumi_df['Luminosity']

    st.plotly_chart(lumiblock_plotly([(t1, y1, 'Lumi')], 
                                     lbn_sb_fsm_flags_df, 
                                     since_dt, 
                                     till_dt,  
                                     'Lumi vs. Lumiblock', 
                                     'Luminosity (W)',
                                     True, 
                                     'LumiBlock Number', 
                                     .03,
                                     [sb_lb_start], 
                                     [sb_lb_end],
                                     False,
                                     percentages),
                   use_container_width = True)

    with st.expander(f"Show FSM Flags dataframe ({mdt_fsm_flags_df.shape[0]})"):

        st.dataframe(lbn_sb_fsm_flags_df,
                     hide_index = True, 
                     use_container_width = True)

        if st.button('Requery FSM Data'):
    
            os.remove(fsm_csv_path) 
            st.rerun()

else:

    if mdt_fsm_flags_df.shape[0] > 0:
        
        lbn_lumi_df = generate_lumi_lbn_df(since_ts, till_ts)

        lbn_fsm_df = generate_lbn_fsm_df(since_dt.tz_convert(None), 
                                         till_dt.tz_convert(None), 
                                         mdt_fsm_flags_df,
                                         st.session_state.chamber_list)

        lbn_lumi_df.to_parquet(path + '/csv/' + 'lbn_lumi_df' + '.parquet')
        lbn_fsm_df.to_parquet(path + '/csv/' + 'lbn_fsm_df' + '.parquet')

        if sb_df.shape[0] >= 2:

            if sb_df.iloc[0]['value'] == 0:

                sb_df = sb_df.iloc[1:]

            if sb_df.iloc[-1]['value'] == 1:

                sb_df = sb_df.iloc[:-1]

            sb_df.reset_index(drop=True, inplace=True)

        if sb_df.shape[0] >= 2:
            
            sb_starts = sb_df[sb_df['value'] == 1]['dt'].tolist()
        
            sb_ends = sb_df[sb_df['value'] == 0]['dt'].tolist()

            # sb_starts.to_parquet(path + '/csv/' + 'sb_starts' + '.parquet')
            # sb_ends.to_parquet(path + '/csv/' + 'sb_ends' + '.parquet')

            t1 = lbn_lumi_df['LumiBlock dt']
            y1 = lbn_lumi_df['Luminosity']

            st.plotly_chart(lumiblock_plotly([(t1, y1, 'Lumi')], 
                                             lbn_fsm_df, 
                                             since_dt, 
                                             till_dt, 
                                             'Lumi vs. Time', 
                                             'Luminosity (W)', 
                                             True, 
                                             'Date', 
                                             .03, 
                                             sb_starts, 
                                             sb_ends, 
                                             True))

        else:

            t1 = lbn_lumi_df['LumiBlock dt']
            y1 = lbn_lumi_df['Luminosity']
        
            st.plotly_chart(lumiblock_plotly([(t1, y1, 'Lumi')], 
                                             lbn_fsm_df, 
                                             since_dt, 
                                             till_dt, 
                                             'Lumi vs. Time', 
                                             'Luminosity (W)', 
                                             True, 
                                             'Date', 
                                             .03, 
                                             None, 
                                             None, 
                                             True))
        
        with st.expander(f"Show FSM Flags dataframe ({lbn_fsm_df.shape[0]})"):

            st.dataframe(lbn_fsm_df,
                         hide_index = True, 
                         use_container_width = True)
            
            st.write(f'{lbn_fsm_df.shape[0]} FSM flags for this time range.')

            if st.button('Requery FSM Data'):
        
                os.remove(fsm_csv_path) 
                st.rerun()

    else:

        lbn_sb_fsm_flags_df = empty_df
        lbn_sb_fsm_flags_df.to_parquet(path + '/csv/' + 'lbn_sb_fsm_flags_df' + '.parquet')

        with st.expander(f"Show FSM Flags dataframe ({lbn_sb_fsm_flags_df.shape[0]})"):

            st.dataframe(lbn_sb_fsm_flags_df,
                         hide_index = True, 
                         use_container_width = True)
            
            if st.button('Requery FSM Data'):
        
                os.remove(fsm_csv_path) 
                st.rerun()

#list the MROD stopless removal and MROD Busy

st.write('## MROD busy and stopless removal')

tmp_path = path + '/csv/' + 'mdt_mrod_df' + '.parquet'

isExist = os.path.exists(tmp_path)

if not isExist:

    try:
        #st.session_state.run_num
        # Convert to UTC and then to a Unix timestamp in microseconds
        mdt_mrod_df_raw = db.get_data('DCS', 
                                      'ATLAS_PVSSMDT', 
                                      'value-number', 
                                      '.*mdtMrod_.*.Status.*', 
                                      True, 
                                      since_ts, 
                                      till_ts, 
                                      0, 
                                      True)
        
        # function to analysis the MROD dataframe
        mdt_mrod_df = queryDataToDataFrame(mdt_mrod_df_raw)
        # crate date column
        mdt_mrod_df['dt'] = pd.to_datetime(mdt_mrod_df['ts'], unit = 'us', utc = True).dt.tz_convert('Europe/Zurich')
        mdt_mrod_df['value'] = mdt_mrod_df['value'].astype(float).astype(int)
        
        mdt_mrod_df.to_parquet(tmp_path)

    except:

        mdt_mrod_df = empty_df
        mdt_mrod_df.to_parquet(tmp_path)

else:

    mdt_mrod_df = pd.read_parquet(tmp_path)

busy_df = mdt_mrod_df[mdt_mrod_df['datapoint'].str.contains('Busy')]
removed_df = mdt_mrod_df[mdt_mrod_df['datapoint'].str.contains('Removed')]

if busy_df.shape[0] != 0:
    
    busy_times_df, total_busy_time = generate_tower_flag_durations_df(busy_df, 'Busy', till_ts, till_dt)

else:
    
    busy_times_df = empty_df
    total_busy_time = 0
    
if removed_df.shape[0] != 0:

    removed_times_df, total_removed_time = generate_tower_flag_durations_df(removed_df, 'Removed', till_ts, till_dt)

else:

    removed_times_df = empty_df
    total_removed_time = 0
        
with st.expander(f"Show MROD Busy and Stopless Removal dataframes ({busy_times_df.shape[0] + removed_times_df.shape[0]})"):
    
    st.dataframe(busy_times_df, hide_index=True, use_container_width = True)
    st.write(f"Total Busy time : {total_busy_time}")
    
    st.dataframe(removed_times_df, hide_index=True, use_container_width = True)
    st.write(f"Total Removal time : {total_removed_time}")

    if st.button('Requery MROD Stopless Removal Data'):

        os.remove(tmp_path) 
        st.rerun()

st.write("## Drop and AutoRecovery")

tmp_path = path + '/csv/' + 'mdt_ddc_df' + '.parquet'

isExist = os.path.exists(tmp_path)

if not isExist:

    try:
        
        # function to analysis the Drop dataframe
        mdt_ddc_df_raw = db.get_data('DCS', 
                                     'ATLAS_PVSSMDT', 
                                     'value-number', 
                                     '.*mdtDDC_.*.Status.*', 
                                     True, 
                                     since_ts, 
                                     till_ts, 
                                     0, 
                                     True)
        
        mdt_ddc_df = queryDataToDataFrame(mdt_ddc_df_raw)
        
        # crate date column
        mdt_ddc_df['dt'] = pd.to_datetime(mdt_ddc_df['ts'], unit = 'us', utc = True).dt.tz_convert('Europe/Zurich')
        mdt_ddc_df['value'] = mdt_ddc_df['value'].astype(float).astype(int)
        mdt_ddc_df['type'] = mdt_ddc_df['datapoint'].apply(lambda x : x.split('.')[-1])
            
        mdt_ddc_df.to_parquet(path + '/csv/' + 'mdt_ddc_df' + '.parquet')

    except:

        mdt_ddc_df = empty_df
        mdt_ddc_df.to_parquet(tmp_path)

else:

    mdt_ddc_df = pd.read_parquet(tmp_path)

condition = mdt_ddc_df['value'] > 0
dropped_df = mdt_ddc_df[condition]
dropped_df.sort_values(by='ts', ascending = False, inplace = True)

st.write("#### Pathologic Chambers")

pathologic_df = dropped_df[dropped_df.type == "Pathologic"].reset_index(drop=True)
pathologic_df.sort_values(by='ts', inplace = True)

if pathologic_df.shape[0] == 0:

    pathologic_df = pd.DataFrame({'Flag': [], 'Date' : [], 'Value' : []})

with st.expander(f"Show Pathologic Chambers dataframe ({pathologic_df.shape[0]})"):

    st.dataframe(pathologic_df, hide_index=True, use_container_width = True)

    if st.button('Requery Pathologic Chambers Data'):

        os.remove(tmp_path) 
        st.rerun()

st.write("#### AutoRecovery Chambers")

autorecovery_df = mdt_ddc_df[mdt_ddc_df['type'] == "Recovering"].reset_index(drop=True)
autorecovery_df.sort_values(by='ts', inplace = True)

if autorecovery_df.shape[0] != 0:

    autorecovery_times_df, total_autorecover_time = generate_chamber_flag_durations_df(autorecovery_df, 'Recovery', till_ts, till_dt)

else:

    autorecovery_times_df = pd.DataFrame({'Flag': [], 'Date' : [], 'Value' : []})
    total_autorecover_time = 0

with st.expander(f"Show Autorecovery Chambers dataframe ({autorecovery_times_df.shape[0]})"):

    st.dataframe(autorecovery_times_df, hide_index=True, use_container_width = True)
    st.write(f"Total Autorecovery time : {total_autorecover_time}")

    if st.button('Requery Autorecovery Chambers Data'):

        os.remove(tmp_path) 
        st.rerun()    

st.write("#### Dropped Chambers")

dropChamber_df = mdt_ddc_df[mdt_ddc_df.type == "Dropped"].reset_index(drop=True)
dropChamber_df.sort_values(by='ts', inplace = True)

if dropChamber_df.shape[0] != 0:

    dropChamber_times_df, total_dropped_time = generate_chamber_flag_durations_df(dropChamber_df, 'Drop', till_ts, till_dt)
    
else:

    dropChamber_times_df = pd.DataFrame({'Flag': [], 'Date' : [], 'Value' : []})
    total_dropped_time = 0

with st.expander(f"Show Dropped Chambers dataframe ({dropChamber_times_df.shape[0]})"):

    st.dataframe(dropChamber_times_df, hide_index=True, use_container_width = True)
    st.write(f"Total Dropped time : {total_dropped_time}")
    
    if st.button('Requery Dropped Chambers Data'):

        os.remove(tmp_path) 
        st.rerun()            
        
st.write("#### Dropped Mezz")

dropMezz_df = mdt_ddc_df[mdt_ddc_df['type'] == "DroppedMezz"].reset_index(drop=True)
dropMezz_df.sort_values(by='ts', inplace = True)
dropMezzNum_df = mdt_ddc_df[mdt_ddc_df['type'] == "DroppedMezzNum"].reset_index(drop=True)
dropMezzNum_df.sort_values(by='ts', inplace = True)
dropMezzMask_df = mdt_ddc_df[mdt_ddc_df['type'] == "DroppedMezzMask"].reset_index(drop=True)
dropMezzMask_df.sort_values(by='ts', inplace = True)

if dropMezz_df.shape[0]:

    dropMezz_times_df, total_dropMezz_time = generate_mezz_flag_durations_df(dropMezz_df, dropMezzNum_df, dropMezzMask_df, till_ts, till_dt)
        
else:

    dropMezz_times_df = pd.DataFrame({'Flag': [], 'Date' : [], 'Value' : []})
    total_dropMezz_time = 0

with st.expander(f"Show Dropped Mezz dataframe ({dropMezz_times_df.shape[0]})"):

    st.dataframe(dropMezz_times_df, hide_index=True, use_container_width = True)
    st.write(f"Total Dropped Mezz time : {total_dropMezz_time}")

    if st.button('Requery Dropped Mezz Data'):

        os.remove(tmp_path) 
        st.rerun()

st.write("## Unplugged Flags")

tmp_path = path + '/csv/' + 'mdt_unplugged_df' + '.parquet'

isExist = os.path.exists(tmp_path)

if not isExist:

    try:
    
        mdt_unplugged_queryData = db.get_data('DCS', 
                                              'ATLAS_PVSSMDT', 
                                              'value-number', 
                                              '.*unplugged', 
                                              True, 
                                              since_ts, 
                                              till_ts, 
                                              0, 
                                              True)
        
        mdt_unplugged_df = queryDataToDataFrame(mdt_unplugged_queryData)
        mdt_unplugged_df.sort_values(by=['ts'], inplace=True)
        mdt_unplugged_df['value'] = mdt_unplugged_df['value'].astype(float).astype(int)
        mdt_unplugged_df['dt'] = pd.to_datetime(mdt_unplugged_df['ts'], unit = 'us').dt.tz_localize('UTC').dt.tz_convert('Europe/Zurich')

        mdt_unplugged_df.to_parquet(tmp_path)

    except:

        mdt_unplugged_df = empty_df
        mdt_unplugged_df.to_parquet(tmp_path)

else:

    mdt_unplugged_df = pd.read_parquet(tmp_path)

unplugged_times_df, total_unplugged_time = generate_unplugged_flag_durations_df(mdt_unplugged_df, st.session_state.pbeast_mapping_df)

with st.expander(f"Show Unplugged dataframe ({unplugged_times_df.shape[0]})"):

    st.dataframe(unplugged_times_df, 
                 hide_index = True, 
                 use_container_width = True)
    st.write(f"Total Unplugged time : {total_unplugged_time}")

    if st.button('Requery Unplugged Data'):

        os.remove(tmp_path) 
        st.rerun()

# except RuntimeError:

#     st.write('## P-BEAST server currently unavailable!')